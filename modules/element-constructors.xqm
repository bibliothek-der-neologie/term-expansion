xquery version "3.1";
(:~  
 : ELEMENT Module ("elements", "http://dev.arokis.com/exist/term_expansion/elements")
 : *******************************************************************************************
 : This module contains element constructors for use in the app
 :
 : @version 0.1 (2019-07-09)
 : @status developing
 : @author Uwe Sikora
 :)
module namespace elements="http://dev.arokis.com/exist/term_expansion/elements";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";


(:######################## Element Constructors ########################:)

(:~  
 : main:element-constructor() [texpan:element-constructor()]
 : constructor function creating a element in the texpan-schema namespace
 :
 : @param $element-name name of constructed element
 : @param $attributes attributes of constructed element
 : @param $content the content of the constructed element
 : @return ts:error node()
 : 
 : @version 0.2 (2018-07-09)
 : @status finished
 : @author Uwe Sikora
 :)
declare function elements:element-constructor
    ( $element-name as xs:string, $attributes as attribute()*, $content as item()* ) as item()* {
    
    element {fn:QName("http://dev.arokis.com/exist/term_expansion/schema", $element-name)} {
        $attributes,
        $content
    }
};


(:~  
 : main:error-element [texpan:error-element()]
 : constructor representing a ts:error element
 :
 : @param $attributes attributes of constructed ts:error
 : @param $content the content of the constructed ts:error
 : @return ts:error node()
 : 
 : @version 0.2 (2019-03-19)
 : @status finished
 : @author Uwe Sikora
 :)
declare function elements:error-element
    ( $attributes as attribute()*, $content ) as item()* {
    
    let $when := attribute when { current-dateTime() }
    return elements:element-constructor("error", ($attributes, $when), (
        $content
        )
    )
};


(:~  
 : main:resource-element() [texpan:resource-element()]
 : constructor representing a ts:resource element
 :
 : @param $attributes attributes of constructed ts:resource
 : @param $content the content of the constructed ts:resource
 : @return ts:resource node()
 : 
 : @version 0.2 (2018-07-09)
 : @status finished
 : @author Uwe Sikora
 :)
declare function elements:resource-element
    ( $attributes as item()*, $content as item()* ) as item()* {
    
    let $when := attribute when { current-dateTime() }
    return elements:element-constructor("resource", ($attributes), $content)
};


(:~  
 : main:word-element() [texpan:word-element()]
 : constructor representing a ts:word element
 :
 : @param $key the key of a ts:word
 : @param $content the content of the constructed ts:word that are ts:term instances
 : @return ts:word node()
 : 
 : @version 0.3 (2018-07-11)
 : @status finished
 : @author Uwe Sikora
 :)
declare function elements:word-element
    ( $key as item()*, $content as item()* ) as item()* {
    
    let $when := attribute when { current-dateTime() }
    let $att-key := attribute key { $key }
    let $att-var-count := attribute variants { count($content) }
    return elements:element-constructor("word", ($att-key, $att-var-count), $content)
};


(:~  
 : main:lemma-element() [texpan:lemma-element()]
 : constructor representing a ts:lemma element
 :
 : @param $key the key of a ts:lemma
 : @param $content the content of the constructed ts:lemma that are ts:term instances
 : @return ts:lemma node()
 : 
 : @version 0.3 (2018-07-11)
 : @status finished
 : @author Uwe Sikora
 :)
declare function elements:lemma-element
    ( $key as item()*, $content as item()* ) as item()* {
    
    let $when := attribute when { current-dateTime() }
    let $att-key := attribute key { $key }
    let $att-var-count := attribute variants { count($content) }
    return elements:element-constructor("lemma", ($att-key, $att-var-count), $content)
};
