xquery version "3.1";
(:~  
 : DOXUMENTATION Module ("documentation", "http://dev.arokis.com/exist/term_expansion/documentation")
 : *******************************************************************************************
 : This module contains functions to handle documentation of the app
 :
 : @version 0.4 (2019-07-09)
 : @status developing
 : @author Uwe Sikora
 :)
module namespace documentation="http://dev.arokis.com/exist/term_expansion/documentation";

import module namespace console="http://exist-db.org/xquery/console";
import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
import module namespace appconf="http://dev.arokis.com/exist/term_expansion/appconf" at "xmldb:exist:///db/apps/term/modules/app-conf.xqm";

declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";
declare namespace tei="http://www.tei-c.org/ns/1.0";

(:############################# Modules Variables #############################:)

(:~  
 : $main:system-data-root
 : The data-root in the system path of eXist 
 : 
 : @version 0.1 (2018-03-15)
 : @author Uwe Sikora
 :)
declare variable $documentation:documentation-data-root := appconf:get-param($appconf:config, "app_documentation-data-collection")/text();

(:############################# Modules Functions #############################:)

declare function documentation:get-docs
    () as item()* {
    collection( $documentation:documentation-data-root )//ts:docs
};

declare function documentation:get-articles
    () as item()* {
    documentation:get-docs()//ts:article
};


declare function documentation:get-articles-ids
    () as item()* {
    data(documentation:get-articles()/@xml:id)
};


declare function documentation:get-articles-tags
    ( ) as item()* {
    distinct-values( documentation:get-articles()/tokenize(@tag, ' ') )
};


declare function documentation:group-articles-by-tag
    ( ) as item()* {
    for $tag in documentation:get-articles-tags()
    return 
        <ts:tag code="{$tag}">
        {
         documentation:get-articles-by-tag( $tag )
        }
        </ts:tag>
};


declare function documentation:get-articles-by-tag
    ( $tag as item() ) as item()* {
    documentation:get-articles()[$tag = tokenize(@tag, ' ')]
};


declare function documentation:get-article-by-id
    ( $id as xs:string ) as item()* {
    documentation:get-articles()[@xml:id = $id]
};


declare function documentation:get-article-tags
    ( $article as item() ) as item()* {
    $article/tokenize(@tag, ' ') 
};

declare function documentation:get-article-author
    ( $article as item() ) as item()* {
    $article/ts:header/ts:author
};

declare function documentation:get-article-title
    ( $article as item() ) as item()* {
    $article/ts:header/ts:title/string()
};

declare function documentation:get-article-date
    ( $article as item() ) as item()* {
    $article/ts:header/ts:date
};
