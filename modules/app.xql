xquery version "3.1";

module namespace app="http://dev.arokis.com/exist/term_expansion/templates";

import module namespace templates="http://exist-db.org/xquery/templates" ;
import module namespace config="http://dev.arokis.com/exist/term_expansion/config" at "config.xqm";
import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
import module namespace termlist="http://dev.arokis.com/exist/term_expansion/termlist" at "xmldb:exist:///db/apps/term/modules/termlists.xqm";
import module namespace documentation="http://dev.arokis.com/exist/term_expansion/documentation" at "xmldb:exist:///db/apps/term/modules/documentation.xqm";

declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";
(:~
 : This is a sample templating function. It will be called by the templating module if
 : it encounters an HTML element with an attribute: data-template="app:test" or class="app:test" (deprecated). 
 : The function has to take 2 default parameters. Additional parameters are automatically mapped to
 : any matching request or function parameter.
 : 
 : @param $node the HTML node with the attribute which triggered this call
 : @param $model a map containing arbitrary data - used to pass information between template calls
 :)
declare function app:test($node as node(), $model as map(*)) {
    <p>Dummy template output generated by function app:test at {current-dateTime()}. The templating
        function was triggered by the data-template attribute <code>data-template="app:test"</code>.</p>
};

declare function app:term-list($node as node(), $model as map(*)) {
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
              <th>#</th>
              <th>TERM</th>
              <th>WORD</th>
              <th>LEMMA</th>
              <th>POS-TAG</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>#</th>
              <th>TERM</th>
              <th>WORD</th>
              <th>LEMMA</th>
              <th>POS-TAG</th>
            </tr>
        </tfoot>
        <tbody>
            {
                let $terms := main:get-terms()
                let $count := count( $terms )
                for $term at $nr in $terms
                return
                    <tr id="{$term/@key}">
                       <td>
                           {$nr}
                       </td> 
                       <td>
                           {$term/text()}
                       </td> 
                       <td>
                           {data($term/@word)}
                       </td> 
                       <td>
                           {data($term/@lemma)}
                       </td> 
                       <td>
                           {data($term/@tag)}
                       </td> 
                    </tr>
            }
        </tbody>
    </table>
};



declare function app:cards-template
    ($node as node(), $model as map(*), $type) {
    
    if ( $type = 'terms' ) then (
        app:cards-standard-component( "Terms", "primary", "fas fa-calendar", count(main:get-terms()) )
    ) else if ( $type = 'words' ) then (
        app:cards-standard-component( "Words", "success", "fas fa-clipboard-list", count(main:get-words()) )
    ) else if ( $type = 'lemma' ) then (
        app:cards-standard-component( "Lemmata", "info", "fas fa-clipboard-list", count(main:get-lemmata()) )
    ) else if ( $type = 'texts' ) then (
        app:cards-standard-component( "Texts indexed", "warning", "fas fa-comments", count( main:list-resource-instances() ) )
    ) else ()
};


declare function app:cards-standard-component
    ( $label as xs:string, $style as xs:string, $icon as xs:string, $value as item() ) {
    
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-{ $style } shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-{$style} text-uppercase mb-1">{ $label }</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{ $value }</div>
            </div>
            <div class="col-auto">
              <i class="{$icon} fa-2x text-gray-300"/>
            </div>
          </div>
        </div>
      </div>
    </div>
};


declare function app:cards-progress-component
    ( $label as xs:string, $style as xs:string, $icon as xs:string, $value as item() ) {
    
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-{ $style } shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-{ $style } text-uppercase mb-1">{ $label }</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{ $value }%</div>
                </div>
                <div class="col">
                  <div class="progress progress-sm mr-2">
                    <div class="progress-bar bg-{ $style }" role="progressbar" style="width: { $value }%" aria-valuenow="{ $value }" aria-valuemin="0" aria-valuemax="100"/>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <i class="{$icon} fa-2x text-gray-300"/>
            </div>
          </div>
        </div>
      </div>
    </div>
};


declare function app:installed-term-lists-list-component
    ( $node as node(), $model as map(*) ) {
    
    <div class="col-xl-4 col-lg-5" style="height: 100%">
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">Available Termlists</h6>
          <div class="dropdown no-arrow">
            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              <div class="dropdown-header">Actions:</div>
              <a class="dropdown-item" href="#">Create Termlist</a>
              <a class="dropdown-item" href="#">Upload Termlist</a>
              <!--<div class="dropdown-divider"></div>-->
            </div>
          </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
            <ul class="list-group">
                { 
                    for $list in termlist:get-term-lists() 
                    let $stats := termlist:get-term-list-stats($list)
                    let $closed := $stats?closed
                    let $ignore := $stats?ignore
                    let $terms := $stats?terms
                    let $name := $stats?name
                    let $closed-icon := if($closed = "true") then ("lock") else ("lock-open")
                    let $ignore-icon := if($ignore = "true") then ("eye-slash") else ("eye")
                    return 
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            { if( xs:integer(string-length($name)) > 30 ) then ( substring($name, 1, 29)||" ..." ) else ($name) }
                            <span>
                                <span class="badge badge-primary badge-pill" style="margin-right: 5px;">{$terms}</span>
                                <!--<i class="fas fa-{if($list?closed = "true") then ("lock") else ("lock-open")} text-{if($list?closed = "true") then ("black") else ("green")}-400"></i>-->
                                <a data-closed="{$closed}" style="margin-right: 5px;" onclick="updateTermListStatus(this, '{$name}')" href="#" class="btn btn-{if($closed = "true") then ("warning") else ("success")} btn-circle btn-sm">
                                    <i class="fas fa-{$closed-icon}"></i>
                                </a>
                                <a data-ignore="{$ignore}" onclick="updateTermListIgnore(this, '{$name}')" href="#" class="btn btn-{if($ignore = "true") then ("warning") else ("success")} btn-circle btn-sm">
                                    <i class="fas fa-{$ignore-icon}"></i>
                                </a>
                            </span>
                        </li>
                }
            </ul>
        </div>
      </div>
    </div>
};

(: #########################################################################################################
 : #    Documentation template functions
 : ######################################################################################################### :)

declare function app:get-doc-article
    ( $node as node(), $model as map(*) ) {
    let $query := request:get-parameter("article", ())
    return <div>{$query}</div>
};

declare function app:list-doc-articles-list
    ( $node as node(), $model as map(*) ) {
    let $articles-by-tag := documentation:group-articles-by-tag()
    return 
        <div class="col-xl-12 col-md-12 mb-12">
          <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Articles by Tags</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    <ul class="list-group">
                    {
                    for $tag in $articles-by-tag
                    let $tag-name := data($tag/@code)
                    let $articles := $tag/ts:article
                    return 
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            {data($tag/@code)}
                        </li>
                    }
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
};

declare function app:list-doc-articles
    ( $node as node(), $model as map(*) ) {
    for $article in documentation:get-articles()
    let $title := documentation:get-article-title($article)
    let $date := xs:date(documentation:get-article-date($article)/@when)
    let $body := $article//ts:body
    order by $date
    return
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">{$title}</h6>
            </div>
            <div class="card-body">
              {$body}
            </div>
      </div>
};
