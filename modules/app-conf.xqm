xquery version "3.1";
(:~  
 : DATASTORE Module ("store", "http://dev.arokis.com/exist/term_expansion/store")
 : *******************************************************************************************
 : This module containes functions to structure the data storage of the app
 :
 : @version 0.4 (2019-07-09)
 : @status developing
 : @author Uwe Sikora
 :)
module namespace appconf="http://dev.arokis.com/exist/term_expansion/appconf";

import module namespace console="http://exist-db.org/xquery/console";
import module namespace config="http://dev.arokis.com/exist/term_expansion/config" at "xmldb:exist:///db/apps/term/modules/config.xqm";
(:import module namespace functx = "http://www.functx.com";:)

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";

(:############################# Modules Variables #############################:)

declare variable $appconf:app-root := $config:app-root;

declare variable $appconf:app-data-root := $config:data-root;

declare variable $appconf:config-path := $appconf:app-root||"/config.xml";

declare variable $appconf:config := appconf:read-config( $appconf:config-path );

declare variable $appconf:system-data-root := appconf:get-param( $appconf:config, "app_data-collection" )/text();
(:############################# Modules Functions #############################:)


(:~  
 : main:create-data-collection()
 : function creating a data collection if it doesn't exist.
 :
 : @param $root the collection where the new collection should be created
 : @param $collection the collection that is going to be created
 : @return xs:string representing the collection's path 
 : 
 : @version 0.1 (2019-03-19)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function appconf:read-config
    ( $config-path as xs:string ) as node()* {
    doc( $config-path )
};


declare function appconf:read-config
    () as node()* {
    appconf:read-config( $appconf:config-path )
};

declare function appconf:get-param
    ( $key as xs:string ) as node()* {
    
    appconf:read-config()//param[@key = $key]
};


declare function appconf:get-param
    ( $config as node(), $key as xs:string ) as node()* {
    
    $config//param[@key = $key]
};


declare function appconf:set-config-param
    ( $config as node(), $key as xs:string, $content as xs:string ) as node()* {
    
    let $param := appconf:get-param( $config, $key )
    let $values := tokenize($param/@values, " ")
    return 
        if( $content = $values) then (
            update value $param with $content,
            $param
        ) else (
            $param
        )
        
};


declare function appconf:get-config-module
    ( $key as xs:string ) as node()* {
    
     appconf:read-config()//module[@key = $key]
};

declare function appconf:get-config-module
    ( $config as node(), $key as xs:string ) as node()* {
    
    $config//module[@key = $key]
};


declare function appconf:get-config-api
    ( $key as xs:string ) as node()* {
    
     appconf:read-config()//api[@key = $key]
};

declare function appconf:get-config-api
    ( $config as node(), $key as xs:string ) as node()* {
    
    $config//api[@key = $key]
};
