xquery version "3.1";
(:~  
 : MAIN Module ("termutils", "http://dev.arokis.com/exist/term_expansion/main")
 : *******************************************************************************************
 : This module is the main module defining general functions used all over the app
 :
 : @version 0.4 (2019-03-20)
 : @status developing
 : @author Uwe Sikora
 :)
module namespace main="http://dev.arokis.com/exist/term_expansion/main";

import module namespace console="http://exist-db.org/xquery/console";
import module namespace functx = "http://www.functx.com";
(:import module namespace config="http://dev.arokis.com/exist/term_expansion/config" at "xmldb:exist:///db/apps/term/modules/config.xqm";:)
import module namespace appconf="http://dev.arokis.com/exist/term_expansion/appconf" at "xmldb:exist:///db/apps/term/modules/app-conf.xqm";
import module namespace cab="http://dev.arokis.com/exist/term_expansion/cab" at "xmldb:exist:///db/apps/term/modules/cab.xqm";
import module namespace store="http://dev.arokis.com/exist/term_expansion/store" at "xmldb:exist:///db/apps/term/modules/datastore.xqm";
import module namespace elements="http://dev.arokis.com/exist/term_expansion/elements" at "xmldb:exist:///db/apps/term/modules/element-constructors.xqm";

declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";
declare namespace tei="http://www.tei-c.org/ns/1.0";
(:############################# Modules Variables #############################:)

(:declare variable $main:app-config := $store:app-config;:)

(:~  
 : $main:context
 : setting the context data-root of the app
 : 
 : @version 0.1 (2019-03-21)
 : @status okay
 : @author Uwe Sikora
 :)
(:declare variable $main:context := helper:get-config-param($main:app-config, "app_data-context")/text() ;:)

(:~  
 : $main:system-data-root
 : The data-root in the system path of eXist 
 : 
 : @version 0.1 (2018-03-15)
 : @author Uwe Sikora
 :)
(:declare variable $main:system-data-root := $store:system-data-root;:)


(:############################# Modules Functions #############################:)

(:~  
 : main:create-checksum() [texpan:create-checksum()]
 : function creating a checksum from a string
 :
 : @param $resource a textual resource
 : @return a ts:resource node() as attributes-only analysis
 : 
 : @version 0.1 (2018-07-10)
 : @status completed
 : @author Uwe Sikora
 :)
declare function main:create-checksum
    ( $resource as item() ) as item()* {
    
    util:hash($resource, "md5")
};


(:~  
 : main:create-term-id() [texpan:create-term-id()]
 : function creating the id of a ts:term
 :
 : @param $term-instance 1 ts:term instance
 : @return xs:sring a checksum
 : 
 : @version 0.2 (2019-03-19)
 : @status completed
 : @author Uwe Sikora
 :)
declare function main:create-term-id
    ( $term-instance as node() ) as xs:string {
    
    main:create-term-id( data($term-instance), data($term-instance/@word), data($term-instance/@tag), data($term-instance/@lemma) )
    (: main:create-checksum( concat(data($term-instance), $term-instance/@word, $term-instance/@tag, $term-instance/@lemma  ) ) :)
};

declare function main:create-term-id
    ( $term as xs:string, $lemma as xs:string, $word as xs:string, $tag as xs:string) as xs:string {
    
    (:main:create-checksum( concat( $term, $word, $tag, $lemma  ) ):)
    util:uuid( concat( $term, $word, $tag, $lemma  ) )
};


(:~  
 : main:xslt() [texpan:xslt()]
 : wrapper-function around the xslt transform-functionalities
 :
 : @param $resource 1 xml-resource to be transformed
 : @param $xslt-uri 1 uri of a xslt-stylesheet
 : @param $xsl-parameters 0..1 parameter used by the xsl-transformation
 : @param $xsl-attributes 0..1 attributes used by the xsl-transformation
 : @param $xsl-serialization-options 0..1 output options
 : @return a transformed xml-instance
 : 
 : @version 0.1 (2018-06-03)
 : @status okay, testing
 : @author Uwe Sikora
 :)
declare function main:xslt
    ( $resource, $xslt-uri as xs:string, $xsl-parameters as node()?, $xsl-attributes as node()?, $xsl-serialization-options as xs:string? ) as item()* {

    let $output := if ( exists($xslt-uri) ) then ( 
            transform:transform(
                $resource, 
                doc($xslt-uri), 
                $xsl-parameters,
                $xsl-attributes,
                $xsl-serialization-options
            )
        ) else ()
        
    return $output
};


(:~  
 : main:analyse-resources() [texpan:resource-analysis()]
 : function analysing a resource (a string of text) and returning a ts:resource object as analysis report without content
 :
 : @param $resource a resource string
 : @param $tokenize a boolean to tell if the resource's content should be tokenized
 : @return a ts:resource node() as attributes-only analysis
 : 
 : @version 0.1 (2019-03-20)
 : @status okay, testing
 : @author Uwe Sikora
 :)
declare function main:analyse-resources
    ( $resources as item()*, $additional-attributes as attribute()*, $tokenize-content as xs:boolean?, $include-content as xs:boolean? ) as node()* {
    
    for $resource in $resources
    let $token := if ( $tokenize-content ) then ( attribute token { count(tokenize($resource)) } ) else ()
    let $checksum := attribute checksum { main:create-checksum($resource) }
    let $entry := elements:resource-element(
            (
                $checksum,
                $token,
                $additional-attributes
            ), 
            if ( $include-content ) then ( $resource ) else ()
        )
    
    return $entry
};


(:~  
 : main:analyse-resource-strings() [texpan:resource-analysis()]
 : function analysing a resource (a string of text) and returning a ts:resource object as analysis report without content
 :
 : @param $resource a resource string
 : @param $tokenize a boolean to tell if the resource's content should be tokenized
 : @return a ts:resource node() as attributes-only analysis
 : 
 : @version 0.3 (2019-03-19)
 : @status okay, testing
 : @author Uwe Sikora
 :)
declare function main:analyse-resource-strings
    ( $resources as xs:string, $tokenize as xs:boolean? ) as node()* {
    
    main:analyse-resources($resources, (), $tokenize, false())
    (:
    for $resource in $resources
    let $token := if ( $tokenize ) then ( attribute token { count(tokenize($resource)) } ) else ()
    let $checksum := attribute checksum { main:create-checksum($resource) }
    let $entry := main:resource-element(
            (
                $checksum,
                $token
            ), 
            ()
        )
    
    return $entry
    :)
};


(:~  
 : main:analyse-resource-strings-full() [texpan:resource-instance()]
 : function analysing a resource (a string of text) and returning a ts:resource object as analysis report including content
 :
 : @param $resources 1..N textual resources
 : @param $tokenize boolean, if true content is tokenized
 : @return a analysed ts:resource instances incl. tesxtual content
 : 
 : @version 0.2 (2018-07-10)
 : @status okay, testing
 : @author Uwe Sikora
 :)
declare function main:analyse-resource-strings-full
    ( $resources as item()*, $tokenize as xs:boolean? ) as item()* {
    
    for $resource in $resources
    let $resource-instance := elements:resource-element(
            main:analyse-resource-strings($resource, $tokenize)/@*, 
            $resource
        )
    
    return $resource-instance
};


(:~  
 : main:analyse-resources-by-uri() [register:analyse-registered-resource(), texpan:uri-resource-instance(), indexer:analyse-registered-resource()]
 : function returning an analysed uri ts:resource element. Including the resource's content is optional
 :
 : @param $resource-uri 1..N resource URIs
 : @param $include-resource xs:boolean, if true content is included
 : @return 0..n item() a sequence of ts:resource objects
 : 
 : @version 0.3 (2019-03-21)
 : @status okay, testing
 : @author Uwe Sikora
 :)
declare function main:analyse-resources-by-uri
    ( $resource-uris as item()*, $include-resource as xs:boolean? ) as item()* {
    
    for $resource-uri in $resource-uris
    let $resource-document := doc( $resource-uri )
    return 
        if( exists($resource-document) ) then (
            main:analyse-resources($resource-document, attribute uri {$resource-uri}, false(), $include-resource)
        ) else ()
};


(:~  
 : main:process-strings-by-cab() [texpan:cabified-resource-instance()]
 : function sending a sequence of strings to the CAB-webservice, creating a ts:resource object, which includes analysed ts:term objects representing the variant spellings
 :
 : @param $resources a sequence of 1..N strings going to be analysed
 : @return node() a sequence of ts:resource objects incl. ts:term nodes if resource is not empty
 : 
 : @version 0.2 (2019-03-19)
 : @status okay
 : @author Uwe Sikora
 :)
declare function main:process-strings-by-cab
    ( $strings as item()* ) as item()* {
    
    for $resource in $strings
    let $resource-analysis := main:analyse-resource-strings($resource, true())
    where not($resource = '') 
    let $cab-analysis := cab:cabify-resource($resource)
    return elements:resource-element($resource-analysis/@*, $cab-analysis)
};


(:############################# Retrieval Functions #############################:)
(:~  
 : texpan:count-token() [texpan:token()]
 : function calculating all tokens run through the database
 :
 : @return sum of all tokens
 : 
 : @version 0.2 (2018-07-11)
 : @status finished
 : @author Uwe Sikora
 :)

declare function main:count-token
    ( ) as item()* {
    sum( main:list-resource-instances()/xs:integer(@token) )
};


(:~  
 : main:get-resources() [texpan:resources-area()]
 : function that returns the ts:resources nodes() stored in the db
 :
 : @return the ts:resources node()
 : 
 : @version 0.3 (2019-03-18)
 : @status OKAY
 : @author Uwe Sikora
 :)

declare function main:get-resources-chunks
    ( ) as item()* {
    store:collection()//ts:resources[ @ignore = "false" ]
};


(:~  
 : main:get-registered-resources-area() [texpan:registered-resources-area()]
 : function that returns the ts:registered node() 
 : 
 : @return the ts:registered node()
 : 
 : @version 0.3 (2019-03-18)
 : @status OKAY, umschreiben, dass registered an der URI und nicht mehr an einem element festgemacht wird
 : @author Uwe Sikora
 :)

declare function main:get-registered-resource-chunks
    ( ) as item()* {
    main:get-resources-chunks()//ts:registered
};


(:~  
 : main:list-resource-instances() [texpan:list-resource-instances()]
 : function to list all resources incl. chunks 
 :
 : @return a sequence of all ts:resource nodes whether registered or not
 : 
 : @version 0.3 (2019-04-18)
 : @status OKAY
 : @author Uwe Sikora
 :)

declare function main:list-resource-instances
    ( ) as item()* {
    main:get-resources-chunks()//ts:resource[ @ignore = "false" ]
};


(:~  
 : main:list-registered-resource-instances() [texpan:list-registered-resource-instances()]
 : function to list all registered resources.
 :
 : @return 0..1 ts:resource
 : 
 : @version 0.2 (2019-03-18)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function main:list-registered-resource-instances
    ( ) as item()* {
    main:list-resource-instances()[ @uri ]
};


(:~  
 : main:list-resource-instance-by-uri() [register:list-resources-instance-by-uri()]
 : function to list a registered resource by URI.
 :
 : @param $resource-uri URI of a registered resource
 : @return the ts:resource or ()
 : 
 : @version 0.1 (2018-03-02)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function main:list-resource-instance-by-uri
    ( $resource-uri as xs:string ) as item()* {
    main:list-registered-resource-instances()[ @uri eq $resource-uri ]
};


(:~  
 : main:list-resource-instance-by-checksum() [texpan:list-resource-instance-by-checksum()]
 : function to list a resource by checksum.
 :
 : @param $checksum of a resource
 : @return the ts:resource or ()
 : 
 : @version 0.2 (2019-03-18)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function main:list-resource-instance-by-checksum
    ( $checksum as xs:string ) as item()* {
    main:list-resource-instances()[ @checksum = $checksum ]
};


(:~  
 : main:get-resource-object-in-db() [texpan:resource-in-db()]
 : function checking if a string was already indexed and retrieves the ts:resource representation of this string from the database
 :
 : @param $resource th resource checked for existence
 : @return 0..1 ts:resource
 : 
 : @version 0.2 (2018-07-13)
 : @status okay
 : @author Uwe Sikora
 :)
declare function main:get-resource-object-in-db
    ( $resource as xs:string ) as item()* {
    
    let $checksum := main:create-checksum( $resource )
    return main:list-resource-instance-by-checksum( $checksum )
};


(:~  
 : main:non-indexed-resources() [texpan:non-indexed-resources()]
 : function to return all non indexed ts:resource instances
 :
 : @return 0..n ts:resource instances
 : 
 : @version 0.2 (2019-03-19)
 : @status finished
 : @author Uwe Sikora
 :)
declare function main:non-indexed-resources
    ( ) as item()* {
    main:list-resource-instances()[ @uri ][ @indexed = "false" ]
};


(:~  
 : main:get-terms-chunks() [terms:terms()]
 : function that returns the ts:terms nodes() stored in the db
 :
 : @return the ts:terms node()
 : 
 : @version 0.3 (2019-03-18)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function main:get-terms-chunks
    ( ) as item()* {
    store:collection()//ts:terms[ @ignore = "false" ]
};


(:~  
 : main:get-open-terms-chunks()
 : function that returns the ts:terms nodes() stored in the db that are flaged @status = 'open'
 :
 : @return the ts:terms node()
 : 
 : @version 0.1 (2019-03-20)
 : @status testing
 : @author Uwe Sikora
 :)
declare function main:get-open-terms-chunks
    ( ) as item()* {
    main:get-terms-chunks()[ @closed != "true" ]
};


(:~  
 : main:get-terms() [texpan:list-terms()]
 : function collectin all terms from the default database
 : 
 : @return the terms from the database that are not ignored
 : 
 : @version 0.3 (2019-03-19)
 : @status okay
 : @author Uwe Sikora
 :)
declare function main:get-terms
    ( ) as item()* {
    main:get-terms-chunks()//ts:term[ not(@ignore) or @ignore = "false" ]
};


(:~  
 : main:get-term() [terms:term-in-db()]
 : function testing if a term specified by term-string is in the database
 :
 : @param $term the term as xs:string to look up
 : @return matching term or false()
 : 
 : @version 0.2 (2018-07-11)
 : @status okay
 : @author Uwe Sikora
 :)
declare function main:get-term
    ( $term as xs:string ) as item()* {
    
    let $terms := main:get-terms()
    return $terms[text() eq $term]
};


(:~  
 : main:get-term() [terms:term-in-db()]
 : function testing if a term specified by string and its modern orthography is in the database
 :
 : @param $term the term as xs:string to look up
 : @param $word the modern orthography of the term as xs:string
 : @return matching term or false()
 : 
 : @version 0.2 (2018-07-11)
 : @status okay
 : @author Uwe Sikora
 :)
declare function main:get-term
    ( $term as xs:string, $word as xs:string ) as item()* {
    
    let $terms := main:get-terms()
    return $terms[text() eq $term and @word eq $word]
};


(:~  
 : main:get-term() [terms:term-in-db()]
 : function testing if a term specified by string, its modern orthography and its CAB tag is in the database
 :
 : @param $term the term as xs:string to look up
 : @param $word the modern orthography of the term as xs:string
 : @param $tag the CAB part-of-speech tag of the term as xs:string
 : @return matching term or false()
 : 
 : @version 0.1 (2018-02-28)
 : @status okay
 : @author Uwe Sikora
 :)
declare function main:get-term
    ( $term as xs:string, $word as xs:string, $tag as xs:string ) as item()* {
    
    let $terms := main:get-terms()
    return $terms[text() eq $term and @word eq $word and @tag eq $tag]
};

declare function main:get-term-opt
    ( $term as xs:string, $word as xs:string?, $tag as xs:string? ) as item()* {
    
    let $terms := main:get-terms()
    return 
        if ($word and $tag) then ( 
            $terms[text() eq $term and @word eq $word and @tag eq $tag]
        ) else if ($word) then ( 
            $terms[text() eq $term and @word eq $word]
        ) else if ($tag) then ( 
            $terms[text() eq $term and @tag eq $tag]
        ) else( 
            $terms[text() eq $term]
        )
};


(:~  
 : texpan:distinct-terms()
 : function to distinct homographouse terms
 :
 : @param $terms a sequence of ts:term objects
 : @return a sequence of 0..n distinct ts:term objects
 : 
 : @version 0.1 (2019-04-19)
 : @status okay
 : @author Uwe Sikora
 :)
declare function main:distinct-terms
    ( $terms as node()* ) as node()* {
    
    for $term in $terms
    group by $term
    return $term
};


(:~  
 : texpan:get-distinct-terms() [texpan:list-distinct-terms()]
 : function collectin all distinct ts:term instances from the database
 :
 : @return a sequence of 0..n ts:term objects
 : 
 : @version 0.2 (2018-07-11)
 : @status okay
 : @author Uwe Sikora
 :)
declare function main:get-distinct-terms
    ( ) as element( ts:term )* {
    
    let $terms := main:get-terms()
    return main:distinct-terms( $terms )
};


(:~  
 : main:get-distinct-terms-from-file()
 : function collectin all distinct ts:term instances from the database
 : 
 : @param $file a path to a ts:texpan file 
 : @return a sequence of 0..n distinct ts:term objects
 : 
 : @version 0.3 (2019-04-19)
 : @status okay
 : @author Uwe Sikora
 :)
declare function main:get-distinct-terms-from-file
    ( $file as xs:string ) as element( ts:term )* {
    
    let $terms := doc($file)//ts:term
    let $distinct := main:distinct-terms( $terms )
    return $distinct
};


(:~  
 : main:group-terms-by-word()
 : function to group ts:term objects by their @word value
 :
 : @param $terms a sequence of ts:term objects
 : @return a sequence of 1..n ts:word objects grouping orthographic variants
 : 
 : @version 0.5 (2019-04-19)
 : @status okay, finished
 : @author Uwe Sikora
 :)
declare function main:group-terms-by-word
    ( $terms as element( ts:term )* ) as element( ts:word )* {
    
    for $term in $terms
    group by $word := $term/@word
    return elements:word-element($word, $term)
};


(:~  
 : main:group-terms-by-lemma()
 : function to group ts:term objects by their @term value
 :
 : @param $terms a sequence of ts:term objects
 : @return a sequence of 1..n ts:lemma objects grouping orthographic variants
 : 
 : @version 0.5 (2019-04-19)
 : @status okay, finished
 : @author Uwe Sikora
 :)
declare function main:group-terms-by-lemma
    ( $terms as element( ts:term )* ) as element( ts:lemma )* {
    
    for $term in $terms
    group by $lemma := $term/@lemma
    return elements:lemma-element($lemma, $term)
};


(:~  
 : main:get-words() [texpan:words(), terms:words()]
 : function to collect all variant ts:term instances grouped by modern orthography as ts:word instance
 :
 : @return a sequence of 1..n ts:word objects grouping orthographic variants
 : 
 : @version 0.5 (2019-04-19)
 : @status okay, finished
 : @author Uwe Sikora
 :)

declare function main:get-words
    ( ) as element( ts:word )* {
    
    let $terms := main:get-distinct-terms()
    let $words := main:group-terms-by-word( $terms )
    return $words
};


(:~  
 : main:get-words-by-key() [texpan:words()]
 : function to return a ts:word that matches the given key
 :
 : @param $key a string tested against //ts:word/@key
 : @return 0..n ts:word instances matching the given $key
 : 
 : @version 0.2 (2019-03-19)
 : @status okay
 : @author Uwe Sikora
 :)
declare function main:get-words-by-key
    ( $key as xs:string ) as element( ts:word )* {
    
    main:get-words()[ @key = $key ]
};


(:~  
 : main:get-lemmata() [texpan:lemmas()]
 : function to return a sequence of ts:lemma
 :
 : @param (optinal) $context a string to identify which data root should be taken: "system" (default) | "app"
 : @return a sequence of 1..n ts:lemma objects grouping orthographic variants
 : 
 : @version 0.2 (2019-003-19)
 : @status okay
 : @author Uwe Sikora
 :)
declare function main:get-lemmata
    ( ) as element( ts:lemma )* {
    
    let $terms := main:get-distinct-terms( )
    let $lemmata := main:group-terms-by-lemma( $terms )
    return $lemmata
};


(:~  
 : main:get-errors() [texpan:errors()]
 : function to return all ts:error instances documented in the db
 :
 : @return 0..n ts:error instances
 : 
 : @version 0.3 (2019-03-19)
 : @status in construction
 : @author Uwe Sikora
 :)
declare function main:get-errors
    ( ) as item()* {
    
    for $error in store:collection()/ts:error
    group by $context := $error/parent::node()/name()
    return elements:element-constructor(
        "errors", 
        (
            attribute count { count($error) }
        ), 
        $error
    ) 
};


(:~  
 : main:get-synonym-list() [texpan:synonym-list()]
 : function returning a list of all synonyms organised by their modern orthography
 :
 : @param (optinal) $context a string to identify which data root should be taken: "system" (default) | "app"
 : @return 1..n lines containing a string of the encoding [\w]:[\w],[\w] ...
 : 
 : @version 0.1 (2018-07-11)
 : @status finished
 : @author Uwe Sikora
 :)
declare function main:get-synonym-list
    ( ) as item()* {
    
    let $variants := main:get-words()
    for $variant in $variants
    let $alternants := string-join((for $alternant in $variant/node() return data($alternant)), ', ')
    return ($variant/@key || ": " || $alternants)
};


(: 
 : ####################################################################################################################
 : ~~~~~                                                                                                        ~~~~~~~
 : ~~~~~~~~~~~             CONSTRUCTION SIDE: IN WORK                                                       ~~~~~~~~~~~
 : ~~~~~                                                                                                        ~~~~~~~
 : #################################################################################################################### 
 :)


declare function main:indexing-timestamps() as item()*{
    let $terms := main:get-terms()
    let $distinct-values := distinct-values(data($terms/@when))
    for $value in $distinct-values
    order by $value ascending
    return map{"when" : $value, "count" : count($terms[@when = $value])}
};
