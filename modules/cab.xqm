xquery version "3.1";
(:~  
 : CAB Module ("termutils", "http://dev.arokis.com/exist/term_expansion/cab")
 : *******************************************************************************************
 : This module defines general functions to communicate with CAB
 :
 : @version 0.2 (2019-03-19)
 : @status developing
 : @author Uwe Sikora
 :)
module namespace cab="http://dev.arokis.com/exist/term_expansion/cab";

import module namespace console="http://exist-db.org/xquery/console";
import module namespace appconf="http://dev.arokis.com/exist/term_expansion/appconf" at "xmldb:exist:///db/apps/term/modules/app-conf.xqm";

declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";
declare namespace tei="http://www.tei-c.org/ns/1.0";

(:############################# Modules Variables #############################:)

(:~  
 : $cab:base-url
 : The base-url of the CAB WEB service from "DTA" 
 : 
 : @version 0.1 (2018-02-28)
 : @author Uwe Sikora
 :)
declare variable $cab:base-url := appconf:get-config-api('cab')/url/text();


(:~  
 : $cab:sample-token
 : A sample token of the CAB JSON response 
 : 
 : @version 0.1 (2018-02-28)
 : @author Uwe Sikora
 :)
declare variable $cab:sample-json-token := (
    '{
		"hasmorph": 1,
		"text": "Das",
		"moot": {
			"word": "Das",
			"tag": "PDS",
			"lemma": "die"
		},
		"exlex": "Das",
		"lang": [
			"de"
		],
		"f": 230477,
		"msafe": 1,
		"errid": "24980",
		"xlit": {
			"isLatinExt": 1,
			"latin1Text": "Das",
			"isLatin1": 1
		}
	}'
);


(:~  
 : $cab:punctuation
 : punctuation characters
 : 
 : @version 0.1 (2018-03-03)
 : @author Uwe Sikora
 :)
declare variable $cab:punctuation := ('!', '‒', '–', '"', '#', '$','§' , '%', '&amp;', "'", '(', ')', '*', '+', ',', '-', '.', '…', 'ꝛ', '‚', '‘', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '`', '{', '|', '}', '~');

(:############################# Modules Functions #############################:)


declare function cab:cabify-resource 
    ( $data as item() ) as item()*{
    
    let $parsed := cab:result( $data )
    let $terms := cab:terms-from-cab($parsed)
    let $filtered-tokens := cab:filtered-tokens($terms)
    return (
        $filtered-tokens
    )
};


declare function cab:result 
    ( $data as item() ) as item()*{
    
    let $cab-response := cab:http-post($data, ())/body
    let $parsed := cab:parse($cab-response/text(), $cab-response/@mimetype)
    return (
        $parsed
    )
};


(:~  
 : cab:clean-up()
 : function to clean up a given string
 :
 : @param $string a string to be cleaned up
 : @return cleaned xs:string
 : 
 : @version 0.1 (2018-03-03)
 : @status develop
 : @author Uwe Sikora
 :)
declare function cab:clean-up
    ( $string as xs:string ) as xs:string {
    let $t := concat(
        "[", 
        string-join( for $i in $cab:punctuation return replace($i, '(\.|\[|\]|\\|\||\-|\^|\$|\?|\*|\+|\{|\}|\(|\))','\\$1') ),
        "]"
        )
    return (
        replace($string, $t, "")
    )
};

(:~  
 : cab:curl()
 : function to call CURL and POST testual data to the DTA-CAB Webservice
 :
 : @param $text a string send to CAB
 : @param $parameter (not used)
 : @return response from CURL
 : 
 : @version 0.1 (2018-01-29)
 : @status deprecated, use cab:http-post() instead
 : @author Uwe Sikora
 :)
declare function cab:curl
    ( $text as xs:string , $parameter as item()? ) as item() {
    
    let $url := $cab:base-url
    let $params := "fmt=raw&amp;ofmt=json&amp;a=default"
    let $curl := ("curl", 
                "-sS",
                "--request", "POST", 
                "--url", xs:anyURI($url || '?' || $params), 
                "--header", 'content-type: text/plain; charset=utf8',
                "--data", $text)
                
    let $process := process:execute($curl, <options/>)
    return (
        <cab>{$process(:, console:log("Calling CURL "):)}</cab>
    )
};


declare function cab:filtered-tokens
    ( $tokens as node()* ) as item()*{
        
    $tokens[ not(starts-with(@tag, "$") or @tag eq "CARD" or @tag eq "XY") ]
};


(:~  
 : cab:parse-analysis()
 : function to parse the json-response from CAB to a report
 :
 : @param $analysises chunks of analysis-elements
 : @return report as group-element with token-elements
 : 
 : @version 0.1 (2018-03-02)
 : @status working
 : @author Uwe Sikora
 :)
declare function cab:parse-analysis
    ( $analysises as item()* ) as item()*{
        
    try {
        for $analysis in $analysises
        let $parsed := parse-json($analysis)
        let $tokens := $parsed?("body")?*?("tokens")?*
        let $terms := ( for $token in $tokens return cab:term-from-token($token) )
        let $relevant-terms := $terms[ not(starts-with(@tag, "$") or @tag eq "CARD" or @tag eq "XY") ]
        return (
            <group terms="{ count($relevant-terms) }">
                { $relevant-terms }
            </group>
        )
    } catch * {
        <error function="cab:parse-analysis" when="{ current-dateTime() }">Caught error {$err:code}: {$err:description}</error>
    }
    
};


(:~  
 : cab:parse
 : function to parse CABs response
 :
 : @param $data data to parse
 : @param $mimetype mimetype of the data
 : @return parsed result
 : 
 : @version 0.2 (2018-03-13)
 : @status working
 : @author Uwe Sikora
 :)
declare function cab:parse
    ( $data as xs:string, $mimetype as xs:string? ) as item()*{
        
    if ( matches($mimetype, "application/json") ) then (
        parse-json($data)
    )
    else (
        $data
    ) 
};


(:~  
 : cab:http-post()
 : function to POST testual data to the DTA-CAB Webservice and retrieve result
 :
 : @param $text a string send to CAB
 : @param $parameter CAB params (if not set there is a default given)
 : @return response from CAB
 : 
 : @version 0.1 (2018-03-13)
 : @status working
 : @author Uwe Sikora
 :)
declare function cab:http-post
    ( $content as xs:string , $parameter as item()? ) as item()* {
    
    let $parameter := if ($parameter) then ($parameter) else ("fmt=raw&amp;ofmt=json&amp;a=expand"),
        $request-url := xs:anyURI($cab:base-url||"?"||$parameter),
        $persist := false(),
        $request-headers := <headers>
                        <header name="Connection" value="close"/>
                    </headers>
    let $request := httpclient:post($request-url, $content, $persist, $request-headers)
    let $response-headers := $request//httpclient:headers,
        $status-code := $request/@statusCode/string(),
        $response-body := $request//httpclient:body
    
    return (
        $request,
        <cabResponse query="{$request-url}" statusCode="{$status-code}">
            {$response-headers}
            <body>
                {
                    $response-body/@*,
                    if ( $response-body/@encoding = "Base64Encoded") then (
                        attribute {"decoded"}{"true"},
                        $response-body/text() => util:base64-decode()
                        
                    ) else (
                        $response-body/text()
                    )
                }
            </body>
        </cabResponse>
    )
};


(:~  
 : cab:term-from-token
 : function which suites as some kind of reduced CAB token-element-constructor. 
 :
 : @param $json-map the input parsed JSON of a CAB-Token
 : @return parsed and reduced token-element from a CAB-Token map()
 : 
 : @version 0.3 (2018-03-13)
 : @status okay (2018-07-11)
 : @author Uwe Sikora
 :)
declare function cab:term-from-token
    ( $token-map as map() ) as item(){
    
    let $lemma := $token-map("moot")?("lemma")
    let $word := $token-map("moot")?("word")
    let $tag := $token-map("moot")?("tag")
    let $lang := $token-map?("lang")?*
    
    return
        element { fn:QName("http://dev.arokis.com/exist/term_expansion/schema", "term") }{
            if ( count($lang) > 0 ) then ( attribute {"lang"}{ $lang } ) else (),
            attribute {"lemma"}{ cab:clean-up($lemma) },
            attribute {"word"}{ cab:clean-up($word) },
            attribute {"tag"}{ $tag },
            attribute {"ignore"}{ "false" },
            cab:clean-up( $token-map("text") )
        }
};


(:~  
 : cab:terms-from-cab
 : function which returns a ts:term for every token in a cab:body/*/cab:tokens
 :
 : @param $cab-map the parsed CAB JSON-Data
 : @return 1..n ts:token-elements
 : 
 : @version 0.3 (2018-03-13)
 : @status okay (2018-07-11)
 : @author Uwe Sikora
 :)
declare function cab:terms-from-cab
    ( $cab-map as map() ) as item()*{
        
    let $tokens := $cab-map?("body")?*?("tokens")?*
    let $terms := ( for $token in $tokens return cab:term-from-token($token) ) 
    return $terms
};
