xquery version "3.1";
(:~  
 : INDEXER Module ("indexer", "http://dev.arokis.com/exist/term_expansion/indexer")
 : *******************************************************************************************
 : This is the indexing module of TEXPAN and contains functions to index and normalize terms of textual resources 
 : 
 : To-Do:
 : - indexing workflow may be restructurde to accomplish better data-saving: 
 :  (1) analysing textual resource -> bag of terms (BOT) 
 :  (2) Termdeduplication during indexing (maybe more resource efficient) -> see ../queries/test_term-deduplication.xql
 : - don't reindex after each indexing but check for duplicates!
 : - maybe clean the index of duplicates
 :
 : @version 0.5 (2018-07-10)
 : @status develop
 : @author Uwe Sikora
 :)

module namespace indexer="http://dev.arokis.com/exist/term_expansion/indexer";

import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
import module namespace elements="http://dev.arokis.com/exist/term_expansion/elements" at "xmldb:exist:///db/apps/term/modules/element-constructors.xqm";
import module namespace console="http://exist-db.org/xquery/console";
(:import module namespace functx = "http://www.functx.com";:)
 
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";

(:############################# Modules Variables #############################:)


(:############################# Modules Functions #############################:)

(:~  
 : indexer:clean-index-file() [terms:clean-index-file()]
 : function that cleans up the index files
 :
 : @param $file path to the source
 : @return sequence of stores term-elements
 : 
 : @version 0.1 (2018-03-02)
 : @status ATTENTION! deprecated
 : @author Uwe Sikora
 :)
declare %private function indexer:clean-index-file
    ( $file as xs:string ) as item()* {
    
    let $file-tree := doc($file)
    let $sources := $file-tree//source
    let $tree := element{name($file-tree/node())}{
        $file-tree/node()/@*,
        $sources,
        main:distinct-terms($file)
    }
    (:let $tmp-file := xmldb:store($terms:store, "tmp.xml", $tree)
    let $new-file := xmldb:store($terms:store, $terms:file-name, doc($tmp-file)/node())
    let $remove-tmp := xmldb:remove($terms:store, "tmp.xml"):)
    
    return doc($new-file)
};


(:~  
 : indexer:clean-index()
 : cleaning function deduplicating terms in the index (currently under constructions)
 :
 : @param (optional) $context a string to identify which data root should be taken: "system" (default) | "app"
 : @return ()
 : 
 : @version 0.2 (2019-03-19)
 : @status experimentation
 : @author Uwe Sikora
 :)
declare function indexer:clean-index
    ( ) as item()* {
    indexer:clean-index( "system" )
};

declare function indexer:clean-index
    ( $context as xs:string ) as item()* {
    
    let $terms := main:get-terms()
    for $term in $terms
    return if ($term[ text() = $term/preceding-sibling::ts:term/text() ]) then (
        update delete $term
    ) else ()
};


(:~  
 : indexer:expand-nonindexed-resources()
 : function that expands a sequence of xs:strings using CAB if not already indexed by the app
 :
 : @param (optional) $context a string to identify which data root should be taken: "system" (default) | "app"
 : @param $resources sequence of xs:strings to be processed by CAB
 : @return sequence of ts:resource objects containing ts:term objects
 : 
 : @version 0.4 (2019-03-19)
 : @status experimentation, testing
 : @author Uwe Sikora
 :)
declare function indexer:expand-nonindexed-resources
    ( $resources as xs:string* ) as node()* {
  
    indexer:expand-nonindexed-resources( "system", $resources )
};

declare function indexer:expand-nonindexed-resources
    ( $context as xs:string, $resources as xs:string* ) as node()* {
  
    for $resource in $resources
    let $checksum := main:create-checksum($resource)
    where not ( main:list-resource-instance-by-checksum($checksum) )
    return main:process-strings-by-cab($resource)
};


(:~  
 : indexer:add-resource-object()
 : function to add a sequences of ts:resource objects to a ts:resources chunk. 
 :
 : @param $resource-instance a ts:resource instance that should be documented
 : @param $target a target node() the resource-entry is documented in
 : @return the resource-entry
 : 
 : @version 0.3 (2019-03-19)
 : @status OKAY (tested)
 : @author Uwe Sikora
 :)
declare function indexer:add-resource-object
    ( $resource as item(), $target as item() ) as item()* {
    
    let $entry := elements:resource-element(
        (
            $resource/@*, 
            attribute when { current-dateTime() },
            attribute ignore { false() }
        ),()
    )
    let $save-entry := (
        try {
            update insert $entry into $target,
            $entry
        } catch * {
            let $error := elements:error-element(
                (
                    attribute code {"3"},
                    attribute function { "register:save-resource" }
                ), "Caught error "||$err:code||": "||$err:description)
            let $document-error := update insert $error into $target
            return $error
        }
    )
    return $save-entry
};


(:~  
 : indexer:add-term-object() [terms:save()]
 : function saving a Object to the database
 :
 : @param $context a string to identify which data root should be taken: "system" | "app"
 : @param $object a ts:term object as item() 
 : @param (optional) $timestamp a timestamp that will be associated with the object
 : @return a ts:term object representing the saved node() | ts:error object if saving was unsucessfull
 : 
 : @version 0.2 (2019-03-19)
 : @status OKAY (tested)
 : @author Uwe Sikora
 :)
declare function indexer:add-term-object
    ( $context as xs:string, $object as item(), $timestamp as xs:boolean ) as item()* {
    
    let $store-area := main:get-open-terms-chunks()[1]
    let $updated-object := 
        if ( $timestamp ) then (
            element{ fn:QName("http://dev.arokis.com/exist/term_expansion/schema", name( $object )) }{
                $object/@*[not(name(.) eq "saved")],
                attribute {"when"} { current-dateTime() },
                attribute {"key"} { main:create-term-id($object) },
                data($object)
            }
        ) else (
            $object
        )
    
    return (
        try {
            update insert $updated-object into $store-area,
            $updated-object
        } catch * {
            let $error := elements:error-element(
                (
                    attribute code {"3"},
                    attribute function { "indexer:add-term-object" }
                ), "Caught error "||$err:code||": "||$err:description)
            let $document-error := update insert $error into $store-area
            return $error
        }
    )
};


(:~  
 : indexer:extract-content-from-chunked-resources()
 : function that extracts sequences of xs:string from the apps ts:texpan/ts:chunk resource.
 :
 : @param $context a string to identify which data root should be taken: "system" | "app"
 : @param $resources a sequence of 1..n xs:strings to be indexed
 : @param $target the target-node where the ts:resource-instance's statistics are documented
 : @return sequence of 1..n ts:term nodes() saved to the database
 : 
 : @version 0.1 (2019-03-20)
 : @status dev
 : @author Uwe Sikora
 :)

declare function indexer:extract-content-from-chunked-resources
    ( $texpan-xml as node()* ) as xs:string* {
    
    "in development"
};


(:~  
 : indexer:index-term() [terms:save-term()]
 : function indexing a given term. "Indexing" means (1) deciding if the term should be stored -> positive -> (2) saving the ts:term object to the data-storage
 :
 : @param $context a string to identify which data root should be taken: "system" | "app"
 : @param $term-object a ts:term object as item()
 : @return the saved ts:term object
 : 
 : @version 0.2 (2019-03-19)
 : @status OKAY (tested)
 : @author Uwe Sikora
 :)
declare function indexer:index-term
    ( $context as xs:string, $term-object as item() ) as item()? {
    
    let $text := $term-object/text()
    let $word := string($term-object/@word)
    let $tag := string($term-object/@tag)
    let $matching-term := main:get-term($text, $word, $tag)
    (:let $matching-term := texpan:list-terms()[text() = $text][@word = $word]:)
    let $relevant-term := not( $text eq $word )
    return (
        if ( not( $matching-term ) and $relevant-term ) then (
            indexer:add-term-object($context, $term-object, true())
        ) else ()
    )
};


(:~  
 : indexer:index-resources()
 : function that (1) adds an expanded ts:resource object to the database and (2) saves all ts:terms if they don't already exist in the database
 :
 : @param $context a string to identify which data root should be taken: "system" | "app"
 : @param $resources a sequence of 1..n xs:strings to be indexed
 : @param $target the target-node where the ts:resource-instance's statistics are documented
 : @return sequence of 1..n ts:term nodes() saved to the database
 : 
 : @version 0.4 (2019-03-19)
 : @status tested, OKAY
 : @author Uwe Sikora
 :)
declare function indexer:index-resources
    ( $context as xs:string, $resources as xs:string*, $target as item()? ) as item()* {
    
    for $resource in indexer:expand-nonindexed-resources($context, $resources)
    (: save resource to a registered resource or to the general indexed resources :)
    let $add-resource-object-to-index := if ($target[self::ts:resource][@uri]) then (
            indexer:add-resource-object($resource, $target)
        ) 
        else (
            let $resources-chunk := main:get-resources-chunks()[@closed != "true"]
            return indexer:add-resource-object($resource, $resources-chunk)
        )
    
    (: distinct terms :)
    let $distinct-terms := main:distinct-terms( $resource//ts:term ) 
    
    (: save all terms :)
    let $save-terms := ( 
        for $term in $resource//ts:term
        
        return indexer:index-term($context, $term) 
        )
    (: let $reindex := texpan:reindex() :)
    return $save-terms
};

declare function indexer:index-resources
    ( $resources as xs:string*, $target as item() ) as item()* {
    
    indexer:index-resources("system", $resources, $target)
};

declare function indexer:index-resources
    ( $resources as xs:string* ) as item()* {
    
    indexer:index-resources( "system", $resources, () )
};


(:~  
 : register:remove-resource-object-by-checksum()
 : function to remove a ts:resource object from the index using its checksum
 :
 : @param $context a string to identify which data root should be taken: "system" | "app"
 : @param $checksum the checksum of a resource that is going to be deleted
 : @return -
 : 
 : @version 0.1 (2018-07-02)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function indexer:remove-resource-object-by-checksum
    ( $context as xs:string, $checksum as xs:string ) as item()* {
    
    let $entry := main:list-resource-instance-by-checksum( $checksum )
    return (
        if ($entry) then (
            update delete $entry,
            <cleaned>{ $checksum }</cleaned>
        ) else ()
    )
};



(:######################## Functions on registered resources########################:)
(: 
 : Those kind of functions that are used to register permanent resources to the index. 
 : resources are permanent if they are stored in the existdb somewhere.
 : 
 : registration means that the URI of permanent resources is added to the index. Registered resources
 : become the targets of different actions:
 :      
 :    [ 0) registration: they are added to the ts:terms/ts:registered object and flagged @indexed = 'false' ]
 :      1) conversion / chunking: they are converted to the apps chunk format
 :      2) indexing: 
 :          - they are added to the ts:terms/ts:registered object and flagged @indexed = 'true'
 :          - they run through indexer:index-resources()
 :      3) consistency check: they are checked for consistency:
 :          - checks whether the resource with the given URI exists
 :          - checks if the checksum is still the same as registered in the apps index
 :      4) removing: they can be removed from the index
 :      5) setting index status: they can be associated with a status wether they are indexed or not
 :      6) synchronising: they can be checked wether they are in synch to the registerd resources or not. If not, they are updated
 : 
 : 
 : Things we need:
 :  - register an URL where we can retrieve data in the texpan chunk schema
 :)


(:~  
 : indexer:register-resource() [register:register-resource()]
 : function to register a resource to the app. 
 :
 : @param $resource-uri URI of a resource to register
 : @return node() a ts:resource object
 : 
 : @version 0.2 (2019-03-19)
 : @status OKAY, tested
 : @author Uwe Sikora
 :)
declare function indexer:register-resource
    ( $context as xs:string, $resource-uri as xs:string ) as item()* {
    
    let $resource := doc( $resource-uri )
    let $registered-instance := main:list-resource-instance-by-uri( $resource-uri )
    return 
        if ( not($registered-instance) and exists($resource) ) then (
            let $resource-instance := elements:resource-element( 
                (
                    main:analyse-resources-by-uri($resource-uri, false())/@*,
                    attribute indexed { false() }
                ), 
                () 
            )
            return (
                let $add-resource-object-to-index := indexer:add-resource-object( $resource-instance, main:get-registered-resource-chunks() )
                return main:list-resource-instance-by-uri( $resource-uri )
            )
        ) else if ( not( exists($resource) ) ) then (
            elements:error-element(
                (
                    attribute code {"1"},
                    attribute function {"indexer:register-resource"}
                ), $resource-uri || " does not exist!"
            )
        ) else ( 
            $registered-instance
        )
};


(:~  
 : indexer:remove-resource-object-by-uri() [register:remove-resource()]
 : function to remove a ts:resource object from the index using its URI
 :
 : @param $resource-uri a resources URI
 : @return node() a ts:removed object containg the URI of the deleted resource
 : 
 : @version 0.1 (2018-07-02)
 : @status OKAY (tested)
 : @author Uwe Sikora
 :)
declare function indexer:remove-resource-object-by-uri
    ( $resource-uri as xs:string ) as item()* {
    
    let $entry := main:list-resource-instance-by-uri( $resource-uri )
    let $log := console:log($resource-uri)
    return (
        if ($entry) then (
            update delete $entry,
            <removed xmlns="http://dev.arokis.com/exist/term_expansion/schema">{ $resource-uri }</removed>
        ) else ($entry)
    )
};


(:~  
 : indexer:check-registered-resource-consistency-by-uri() [register:resource-consistency-check(), register:resource-consistency()]
 : function to ckeck the consistency of a registered resource:
 :      1) checks whether the resource with the given URI exists
 :      2) checks if the checksum is still the same as registered in the apps index
 :
 : @param $resource the resource that is checked.
 : @return ts:consistency
 : 
 : @version 0.2 (2019-03-19)
 : @status  OKAY (tested)
 : @author Uwe Sikora
 :)
declare function indexer:check-resource-consistency-by-uri
    ( $resource-uri as xs:string ) as item()* {
    
    let $resource-object := main:list-resource-instance-by-uri( $resource-uri )
    let $content := doc( $resource-uri )
    let $exists := exists( $content )
    let $consistency-report := (
        if ( $exists ) then (
            let $checksum := util:hash($content, "md5")
            let $in-synch := $checksum eq $resource-object/@checksum
            return ( 
                element {fn:QName("http://dev.arokis.com/exist/term_expansion/schema", "consistency")} {
                    $resource-object/@*,
                    attribute actualChecksum { $checksum },
                    attribute exists { $exists },
                    attribute synch {$in-synch}
                }
            )
        ) else (
            element {fn:QName("http://dev.arokis.com/exist/term_expansion/schema", "consistency")} {
                    $resource-object/@*,
                    attribute exists { false() },
                    attribute synch { false() }
                }
        )    
    )
    
    return ( $consistency-report, $resource-object )
};


(:~  
 : indexer:set-resource-index-status() [register:set-index-status()]
 : function to set the index status on the node() of a resource. 
 :
 : @param $resource node() of the resource
 : @param $status the indexing status which should be associated with the resource
 : @return the registered resource's entry
 : 
 : @version 0.1 (2018-06-27)
 : @status OKAY (tested)
 : @author Uwe Sikora
 :)
declare function indexer:set-resource-index-status
    ( $resource-object as node(), $status as xs:boolean ) as item()* {
    
    let $set-status := if ( $resource-object[@indexed] ) then (
            update replace $resource-object/@indexed with attribute indexed { $status }
        ) else (
            update insert attribute indexed { $status } into $resource-object
        )
    return $resource-object
};


(:~  
 : indexer:set-resource-index-status-by-uri() [register:set-index-status-by-uri()]
 : function to set the index status of a registered resource identified by its URI
 :
 : @param $resource-uri xs:string URI of the resource
 : @param $status the indexing status which should be associated with the resource
 : @return the registered resource's ts:resource object
 : 
 : @version 0.1 (2018-06-27)
 : @status OKAY (tested)
 : @author Uwe Sikora
 :)
declare function indexer:set-resource-index-status-by-uri
    ( $resource-uri as xs:string, $status as xs:boolean ) as item()* {
    
    let $resource-object := main:list-resource-instance-by-uri( $resource-uri )
    let $set-status := indexer:set-resource-index-status( $resource-object, $status )
    return $resource-object
};


(:~  
 : indexer:synchronise-resource-by-uri() [register:synchronise-resource()]
 : function to synchronise resources. 
 : First of all it is checked if the resource in question is in synch with the actual document identified by the resource-URI. 
 : In case both the resource and its document are not in synch two things happen: 
 :      (1) the old checksum is replaced by a new one calculated from the document-data
 :      (2) the resource is marked as @indexed="false"
 :
 : @param $resource-uri resource-uri of a registered resource as xs:string
 : @return $resource or ()
 : 
 : @version 0.2 (2019-03-19)
 : @status OKAY (tested)
 : @author Uwe Sikora
 :)
declare function indexer:synchronise-resource-by-uri
    ( $resource-uri as xs:string ) as item()* {
    
    let $resource-consistency := indexer:check-resource-consistency-by-uri( $resource-uri )
    let $resource-object := $resource-consistency[2]
    let $resource-document := doc( $resource-uri )
    let $resource-in-synch := xs:boolean( data($resource-consistency[1]/@synch) )
    
    let $synchronised := (
            if ( $resource-object and not( $resource-in-synch ) ) then (
                let $checksum := attribute checksum { util:hash($resource-document, "md5") }
                let $synchronise := update replace $resource-object/@checksum with $checksum
                let $set-index-status := indexer:set-resource-index-status($resource-object, false())
                return (
                    $resource-object
                )
            ) else ()
        )
    
    return $synchronised
};

(: 
 : ####################################################################################################################
 : ~~~~~                                                                                                        ~~~~~~~
 : ~~~~~~~~~~~             CONSTRUCTION SIDE: IN WORK                                                       ~~~~~~~~~~~
 : ~~~~~                                                                                                        ~~~~~~~
 : #################################################################################################################### 
 :)




(: Das hier ist eine funktion aus register.xqm, das perpektivisch hier aufgehen soll:)
(:~  
 : indexer:index-registered-resource()
 : function to index a registered ts:resource instance
 :
 : @param $resource-uri the uri of a resource that should be indexed
 : @param $preprocessing-xslt uri of a xsl stylesheet
 : @param $xslt-parameters xsl parameters used by the transformation
 : @return 1..n stored ts:term instances
 : 
 : @version 0.1 (2018-03-02)
 : @status dev
 : @status-next testing
 : @author Uwe Sikora
 :)
declare function indexer:index-registered-resource
    ( $context as xs:string, $resource-uri, $preprocessing-xslt as xs:string?, $xsl-parameters as node()? ) as item()* {
    
    let $resource-object := indexer:register-resource( $context, $resource-uri ) 
    let $preprocessed-data := main:xslt( doc($resource-uri), $preprocessing-xslt, $xsl-parameters, (), () )//normalize-space(child::node())
    let $indexed-data := indexer:index-resources( $context, $preprocessed-data, $resource-object )
    let $conclude-indexing := indexer:set-resource-index-status-by-uri($resource-uri, true())
    return ($indexed-data)
    
};






(: ####################################################################################################################################
 : #
 : #        Chunking
 : #
 : #################################################################################################################################### :)


declare function indexer:chunk-text-primitive
    ($text, $range) as item()*{
    
    let $token := tokenize($text, ' ')
    let $count := count($token)
    return indexer:chunk-range( $token, $range )
};

declare function indexer:chunk-range
    ( $seq as item()*, $range as xs:integer ) as item()*{
    
    let $count := count($seq)  
    let $sub := subsequence( $seq, $range+1, $count )
    return 
        if ( $count > 0 ) then (
            <ts:chunk count="{$count}">
                { subsequence( $seq, 1, $range ) }
            </ts:chunk>,
            indexer:chunk-range( $sub, $range )
        ) else ()
};





(: ##################################################################################################### :)

(:declare function indexer:distinct-terms:)
(:    ( $terms as element(ts:term) * ) as element(ts:term) *{:)
(:    functx:distinct-deep($terms):)
(:};:)
    
declare function indexer:filter-terms
    ( $terms as element(ts:term) * ) as element(ts:term) *{
    $terms[ not( text() = @word ) ]
};

declare function indexer:not-indexed-terms
    ( $terms as element(ts:term) * ) as element(ts:term) *{
(:    for $term in $terms:)
(:    let $in-db := main:get-term($term/text(), data($term/@word), ()):)
(:    where not( $in-db ):)
(:    return $term:)
    let $terms-in-db := main:get-terms()
    for $term in $terms
    return 
        if( not($terms-in-db[text() eq $term/text()][@word eq $term/@word][@tag eq $term/@tag]) ) then (
            $term
        ) else ()
};

declare function indexer:index-terms
    ( $terms as element(ts:term) * ) as element(ts:term) *{
    
(:    let $distinct-terms := indexer:distinct-terms($terms):)
    let $distinct-terms := main:distinct-terms($terms)
    let $filtered := indexer:filter-terms($distinct-terms)
    let $non-indexed-terms := indexer:not-indexed-terms($filtered)   
(:    let $index := (for $term in $non-indexed-terms return indexer:add-term-object("system", $term, true())):)
(:    return $index:)
    return indexer:add-term-objects("system", $non-indexed-terms, true())
};

declare function indexer:add-term-objects
    ( $context as xs:string, $objects as element(ts:term)*, $timestamp as xs:boolean ) as element(ts:term)* {
    
    let $store-area := main:get-open-terms-chunks()[1]
    let $updated-objects := (
            for $object in $objects
            return 
                if ( $timestamp ) then (
                    element{ fn:QName("http://dev.arokis.com/exist/term_expansion/schema", name( $object )) }{
                        $object/@*[not(name(.) eq "saved")],
                        attribute {"when"} { current-dateTime() },
                        attribute {"key"} { main:create-term-id($object) },
                        data($object)
                    }
                ) else (
                    $object
                )
        )
        
    
    return (
        if ($updated-objects) then (
            try {
                update insert $updated-objects into $store-area,
                $updated-objects
            } catch * {
                let $error := elements:error-element(
                    (
                        attribute code {"3"},
                        attribute function { "indexer:add-term-object" }
                    ), "Caught error "||$err:code||": "||$err:description)
                let $document-error := update insert $error into $store-area
                return $error
            }
        ) else ()
    )
};

declare function indexer:index-resources-fast
    ( $resources as xs:string*, $target ) as item()* {
    
    for $resource in indexer:expand-nonindexed-resources("system", $resources)
    (: save resource to a registered resource or to the general indexed resources :)
    let $add-resource-object-to-index := if ($target[self::ts:resource][@uri]) then (
            indexer:add-resource-object($resource, $target)
        ) 
        else (
            let $resources-chunk := main:get-resources-chunks()[@closed != "true"]
            return indexer:add-resource-object($resource, $resources-chunk)
        )
    
    return indexer:index-terms($resource//ts:term)
};


