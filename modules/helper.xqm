xquery version "3.1";
(:~  
 : HELPER Module ("helper", "http://dev.arokis.com/exist/helper")
 : *******************************************************************************************
 :
 : @version 0.1 (2019-07-03)
 : @status developing
 : @author Uwe Sikora
 :)
module namespace helper="http://dev.arokis.com/exist/helper";
import module namespace config="http://dev.arokis.com/exist/term_expansion/config" at "xmldb:exist:///db/apps/term/modules/config.xqm";


(:############################# Modules Variables #############################:)

(:~  
 : $main:context
 : setting the context data-root of the app
 : 
 : @version 0.1 (2019-03-21)
 : @status okay
 : @author Uwe Sikora
 :)
declare variable $helper:config := $config:app-root||"/config.xml" ;

(:############################# Modules Functions #############################:)


(:~  
 : main:create-data-collection()
 : function creating a data collection if it doesn't exist.
 :
 : @param $root the collection where the new collection should be created
 : @param $collection the collection that is going to be created
 : @return xs:string representing the collection's path 
 : 
 : @version 0.1 (2019-03-19)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function helper:read-config
    ( $config-path as xs:string ) as node()* {
    
    doc( $config-path )
};

declare function helper:read-config
    () as node()* {
    
    doc( $helper:config )
};

declare function helper:get-config-param
    ( $key as xs:string ) as node()* {
    
    helper:read-config()//param[@key = $key]
};


declare function helper:get-config-param
    ( $config as node(), $key as xs:string ) as node()* {
    
    $config//param[@key = $key]
};


declare function helper:set-config-param
    ( $config as node(), $key as xs:string, $content as xs:string ) as node()* {
    
    let $param := helper:get-config-param( $config, $key )
    let $values := tokenize($param/@values, " ")
    return 
        if( $content = $values) then (
            update value $param with $content,
            $param
        ) else (
            $param
        )
        
};


declare function helper:get-config-module
    ( $key as xs:string ) as node()* {
    
     helper:read-config()//module[@key = $key]
};

declare function helper:get-config-module
    ( $config as node(), $key as xs:string ) as node()* {
    
    $config//module[@key = $key]
};


declare function helper:get-config-api
    ( $key as xs:string ) as node()* {
    
     helper:read-config()//api[@key = $key]
};

declare function helper:get-config-api
    ( $config as node(), $key as xs:string ) as node()* {
    
    $config//api[@key = $key]
};
