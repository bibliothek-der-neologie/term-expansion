xquery version "3.1";
(:~  
 : DATASTORE Module ("store", "http://dev.arokis.com/exist/term_expansion/store")
 : *******************************************************************************************
 : This module containes functions to structure the data storage of the app
 :
 : @version 0.4 (2019-07-09)
 : @status developing
 : @author Uwe Sikora
 :)
module namespace store="http://dev.arokis.com/exist/term_expansion/store";

import module namespace console="http://exist-db.org/xquery/console";
import module namespace appconf="http://dev.arokis.com/exist/term_expansion/appconf" at "xmldb:exist:///db/apps/term/modules/app-conf.xqm";
(:import module namespace functx = "http://www.functx.com";:)

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";


(:############################# Modules Functions #############################:)


(:~  
 : main:create-data-collection()
 : function creating a data collection if it doesn't exist.
 :
 : @param $root the collection where the new collection should be created
 : @param $collection the collection that is going to be created
 : @return xs:string representing the collection's path 
 : 
 : @version 0.1 (2019-03-19)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function store:create-data-collection
    ( $root as xs:string, $collection as xs:string ) as xs:string {
    
    let $path := $root || substring-after($collection, "/")
    return if ( not( xmldb:collection-available( $path  ) ) ) then (
        xmldb:create-collection($root, $collection)
    ) else (
        $path
    )
};


(:~  
 : main:log() [texpan:log()]
 : log function to log messages to files
 :
 : @param $log-message the message to be loged
 : @param $log-target the target to log the message to
 : @return console:log of the message 
 : 
 : @version 0.1 (2018-03-02)
 : @status okay
 : @author Uwe Sikora
 :)
declare function store:log
    ( $log-message as item(), $log-target as item() ) as item()* {
    
    update insert $log-message into $log-target,
    console:log( concat( base-uri($log-target), ': "', $log-message, '"') )
};



(:############################# Retrieval Functions #############################:)



(:~  
 : main:get-data-root()
 : function to to get a data root from the app
 :
 : @param (optinal) $context a string to identify which data root should be taken: "system" (default) | "app"
 : @return xs:string the path of the selected data root
 : 
 : @version 0.1 (2018-03-03)
 : @status develop
 : @author Uwe Sikora
 :)
declare function store:get-data-root
    () as xs:string* {
    let $configured-datastore :=  store:check-data-root($appconf:system-data-root)
    return 
        try {
            if ( $configured-datastore ) then (
                $configured-datastore
            ) else (
                $appconf:app-data-root
            )
        } catch * {
            fn:error(QName("http://dev.arokis.com/exist/term_expansion/schema", "error"), "WrongDataContext")
        }
};


(:~  
 : main:check-data-root()
 : function to check if a data-root exists
 :
 : @param $string path of a collection that should be checked
 : @return xs:string the path of the checked data root | an error object
 : 
 : @version 0.1 (2019-04-18)
 : @status develop
 : @author Uwe Sikora
 :)
declare function store:check-data-root
    ( $root as xs:string ) as xs:string* {
    
    let $exists := xmldb:collection-available($root)
(:    let $collection := functx:substring-after-last($root, "/"):)
(:    let $path := functx:substring-before-last($root, "/"):)
    return if ( $exists ) then (
        $root
    ) else (
        fn:error(QName("http://dev.arokis.com/exist/term_expansion/schema", "error"), "CollectionDoesNotExist")
    )
};


declare function store:collection
    () as item() * {
    collection( store:get-data-root() )
};


(: 
 : ####################################################################################################################
 : ~~~~~                                                                                                        ~~~~~~~
 : ~~~~~~~~~~~             CONSTRUCTION SIDE: IN WORK                                                       ~~~~~~~~~~~
 : ~~~~~                                                                                                        ~~~~~~~
 : #################################################################################################################### 
 :)

declare function store:reindex
    ( ) as item()* {
    
    xmldb:reindex($appconf:data-root)
};

declare function store:reindex
    ( $storages as xs:string* ) as item()* {
    
    for $storage in $storages
    return xmldb:reindex($storage)
};







