xquery version "3.1";
(:~  
 : TERMLIST Module ("termlist", "http://dev.arokis.com/exist/term_expansion/termlist")
 : *******************************************************************************************
 : This module is the main module defining general functions used all over the app
 :
 : @version 0.4 (2019-03-20)
 : @status developing
 : @author Uwe Sikora
 :)
module namespace termlist="http://dev.arokis.com/exist/term_expansion/termlist";

import module namespace console="http://exist-db.org/xquery/console";
import module namespace config="http://dev.arokis.com/exist/term_expansion/config" at "xmldb:exist:///db/apps/term/modules/config.xqm";
import module namespace appconf="http://dev.arokis.com/exist/term_expansion/appconf" at "xmldb:exist:///db/apps/term/modules/app-conf.xqm";

declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";




(:############################# Modules Functions #############################:)

(:~  
 : termlist:test-term-list-exists()
 : tests if a termlist already exists in the database
 :
 : @param $name of the termlist
 : @return xs:boolean
 : 
 : @version 0.1 (2019-07-09)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function termlist:test-term-list-exists
    ( $name as xs:string ) as xs:boolean {
    
    let $coll := collection($appconf:system-data-root)/tokenize(base-uri(), "/")[last()]
    return if ($name = $coll ) then (true()) else (false())
 
};


(:~  
 : termlist:create-termlist()
 : creating a termlist in the preconfigured system store
 :
 : @param $root the collection where the new collection should be created
 : @param $collection the collection that is going to be created
 : @return xs:string representing the collection's path 
 : 
 : @version 0.1 (2019-03-19)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function termlist:create-termlist
    ( $name as xs:string ) as item()* {
    let $path := xs:anyURI($appconf:system-data-root||"/"||$name)
    return 
        if ( not(termlist:test-term-list-exists($name) ) ) then (
            let $path := xs:anyURI($appconf:system-data-root||"/"||$name)
            return (
                xmldb:copy( "/db/apps/term/datamodel/templates/", $appconf:system-data-root, "terms.template.xml" ),
                xmldb:rename($appconf:system-data-root, "terms.template.xml", $name),
                sm:chmod($path, "rw-rw-rw-"),
                $path
            )
        ) else (
            $path
        )
    
};


(:~  
 : termlist:delete-termlist()
 : deletes the given termlist
 :
 : @param $name the name of the list going to be removed
 : @return a map containg the stats
 : 
 : @version 0.1 (2019-07-09)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function termlist:delete-termlist
    ( $name as xs:string ) as item()* {
    
    xmldb:remove( $appconf:system-data-root, $name ),
    map{ 
        "action" : "delete-list", 
        "name" : $name, 
        "path" : $appconf:system-data-root
    }
};


(:~  
 : termlist:termlist:get-term-lists()
 : retrieves all termlists of the system
 :
 : @return all termlists as ts:terms node()
 : 
 : @version 0.1 (2019-07-09)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function termlist:get-term-lists
    ( ) as item()* {
    
    collection($appconf:system-data-root)//ts:terms
};


(:~  
 : termlist:get-term-list-by-name()
 : retrieves all termlists of the system
 :
 : @param $name the name of the termlist
 : @return the termlist as ts:terms node()
 : 
 : @version 0.1 (2019-07-09)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function termlist:get-term-list-by-name
    ( $name ) as item()* {
    termlist:get-term-lists()[tokenize(base-uri(), '/')[last()] = $name]
};


(:~  
 : termlist:get-term-list-by-uri()
 : retrieves all termlists of the system
 :
 : @param $name the name of the termlist
 : @return the termlist as ts:terms node()
 : 
 : @version 0.1 (2019-07-09)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function termlist:get-term-list-by-uri
    ( $uri ) as item()* {
    termlist:get-term-lists()[base-uri() = $uri]
};


(:~  
 : termlist:get-term-list-stats()
 : retrieves the stats of given termlists
 :
 : @param $termlists the termlists which the stats will be analysed
 : @return a map containg the stats
 : 
 : @version 0.1 (2019-07-09)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function termlist:get-term-list-stats
    ( $termlists as item()* ) as item()* {
    for $list in $termlists
    let $uri := $list/base-uri()
    let $name := tokenize($uri, '/')[last()]
    let $term-count := count($list/ts:term)
    let $closed := data($list/@closed)
    let $ignore := data($list/@ignore)
    return 
        map{ 
            "uri" : $uri, 
            "terms" : $term-count, 
            "closed" : $closed, 
            "name" : $name, 
            "ignore" : $ignore
        }
};


(:~  
 : termlist:set-term-list-closed-status()
 : sets the closed status of a termlist
 :
 : @param $termlists the termlists which the stats will be analysed
 : @param $closed the status of the closed attribute to set
 : @return the termlist
 : 
 : @version 0.1 (2019-07-09)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function termlist:set-term-list-closed-status
    ( $termlist as item(), $closed as xs:string ) as item() {
    if ( $closed = "false" ) then (
        termlist:open-term-list( $termlist )
    ) else if ( $closed = "true" ) then (
        termlist:close-term-list( $termlist )
    ) else ($termlist)
};

declare function termlist:close-term-list
    ( $termlist as item() ) as item() {
    if ( $termlist/@closed = "false" ) then (
        update replace $termlist/@closed with "true"
    ) else (),
    $termlist
};

declare function termlist:open-term-list
    ( $termlist as item() ) as item() {
    if ( $termlist/@closed = "true") then (
        update replace $termlist/@closed with "false"
    ) else (),
    $termlist
};


(:~  
 : termlist:set-term-list-ignore-status()
 : sets the ignore status of a termlist
 :
 : @param $termlists the termlists which the stats will be analysed
 : @param $ignore the status of the ignore attribute to set
 : @return the termlist
 : 
 : @version 0.1 (2019-07-09)
 : @status OKAY
 : @author Uwe Sikora
 :)
declare function termlist:set-term-list-ignore-status
    ( $termlist as item(), $ignore as xs:string ) as item() {
    if ( $ignore = "false" ) then (
        termlist:do-not-ignore-term-list( $termlist )
    ) else if ( $ignore = "true" ) then (
        termlist:ignore-term-list( $termlist )
    ) else ($termlist)
};

declare function termlist:do-not-ignore-term-list
    ( $termlist as item() ) as item() {
    if ( $termlist/@ignore = "true" ) then (
        update replace $termlist/@ignore with "false"
    ) else (),
    $termlist
};

declare function termlist:ignore-term-list
    ( $termlist as item() ) as item() {
    if ( $termlist/@ignore = "false") then (
        update replace $termlist/@ignore with "true"
    ) else (),
    $termlist
};