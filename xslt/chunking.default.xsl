<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    
     <xsl:param name="chunk-node"/>
    <xsl:param name="type"/>
    
    <xsl:template match="/">
        <xsl:for-each select="//node()[name() eq $chunk-node][@type eq $type]">
            <xsl:copy>
                <xsl:apply-templates/>
            </xsl:copy>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="text()[normalize-space(.) = '']"/>
    
    <xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
        <xsl:text> </xsl:text>
    </xsl:template>

</xsl:stylesheet>