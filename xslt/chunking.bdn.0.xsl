<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://dev.arokis.com/exist/term_expansion/schema" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0" xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    
    <xsl:template match="/">
        <texpan>
            <xsl:for-each select="//tei:div[@type = 'chapter']">
                <xsl:call-template name="chunk"/>
            </xsl:for-each>
        </texpan>
    </xsl:template>
    
    <xsl:template name="chunk">
        <chunk>
            <xsl:apply-templates/>
        </chunk>
    </xsl:template>
    
    <xsl:template match="text()[normalize-space(.) = '']"/>
    
    <xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
        <xsl:text> </xsl:text>
    </xsl:template>

</xsl:stylesheet>