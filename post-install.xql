xquery version "3.0";

import module namespace store="http://dev.arokis.com/exist/term_expansion/store" at "xmldb:exist:///db/apps/term/modules/datastore.xqm";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";

let $data-root := "/db"
let $collection := "data/apps/term/data"

(: 1. Creating the data collection outside the app collection which is used as default data collection :)
let $create-data-collection := store:create-data-collection($data-root, $collection)
let $terms-main := "terms.main.xml"

(: 2. Creating the main storage file for ts:term objects in the default data collection :)
let $create-terms-storage := (
    if ( not(collection($create-data-collection)//ts:terms[@closed="false"]) ) then (
        
        if ( not(doc($store:system-data-root||"/"||$terms-main) ) ) then (
            xmldb:copy( "/db/apps/term/datamodel/templates/", $store:system-data-root, "terms.template.xml" ),
            xmldb:rename($store:system-data-root, "terms.template.xml", "terms.main.xml"),
            sm:chmod(xs:anyURI($store:system-data-root||"/terms.main.xml"), "rw-rw-rw-")
        ) else (
            let $name := "terms."||util:hash(current-dateTime(), "md5")||".xml"
            return (
                xmldb:copy( "/db/apps/term/datamodel/templates/", $store:system-data-root, "terms.template.xml" ),
                xmldb:rename($store:system-data-root, "terms.template.xml", $name),
                sm:chmod(xs:anyURI($store:system-data-root||"/"||$name), "rw-rw-rw-")
            )
        )
    ) else ()    
)

(: 3. Creating the main storage file for ts:resource objects in the default data collection :)
let $create-resources-storage := (
    if ( not(collection($create-data-collection)//ts:resources) ) then (
        xmldb:copy( "/db/apps/term/datamodel/templates/", $store:system-data-root, "resources.template.xml" ),
        xmldb:rename($store:system-data-root, "resources.template.xml", "resources.main.xml"),
        sm:chmod(xs:anyURI($store:system-data-root||"/resources.main.xml"), "rw-rw-rw-")
    ) else ()    
)

(: 4. copying all extension file to the default data collection :)
let $copy-extensions := (
    let $extension-collection := "/db/apps/term/data/extensions"
    return 
        if ( xmldb:collection-available($extension-collection) ) then (
            for $file in collection( $extension-collection )
            let $file-uri := $file/base-uri()
            let $file-name := tokenize( $file-uri, "/")[last()]
            return 
                xmldb:copy( $extension-collection, $store:system-data-root, $file-name )
                sm:chmod(xs:anyURI($store:system-data-root||"/"||$file-name), "rw-rw-rw-")
        ) else (
            "no extensions collection available!"
        )    
)

return (
    $create-data-collection,
    $create-terms-storage, 
    $create-resources-storage,
    $copy-extensions
)
