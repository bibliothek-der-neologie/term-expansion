xquery version "3.1";
(:~  
 : REST-API Module to handle RESOURCES ("restapi", "http://dev.arokis.com/exist/term_expansion/resources_api")
 : *******************************************************************************************
 : This is the API module defining a REST-API to the texpan app. It is used to handle resources (add, remove, synchronise etc.)
 :
 : @version 0.2 (2018-07-10)
 : @status develop
 : @author Uwe Sikora
 :)

module namespace restapi="http://dev.arokis.com/exist/term_expansion/resources_api";
import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
import module namespace termlist="http://dev.arokis.com/exist/term_expansion/termlist" at "xmldb:exist:///db/apps/term/modules/termlists.xqm";
import module namespace indexer="http://dev.arokis.com/exist/term_expansion/indexer" at "xmldb:exist:///db/apps/term/modules/indexer.xqm";
import module namespace console="http://exist-db.org/xquery/console";
import module namespace rest = "http://exquery.org/ns/restxq";
import module namespace transform="http://exist-db.org/xquery/transform";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";
declare namespace http="http://expath.org/ns/http-client";


(:~  
 : READ: restapi:term-lists()
 : lists all termlists of the list
 :
 : @return a json-object listing all termlists
 : 
 : @version 0.3 (2018-07-11)
 : @status finished
 : @author Uwe Sikora
 :)
declare
    %rest:path("terms/api/1.0/termlists")
    %rest:GET
    %output:method("json")
function restapi:term-lists
    () as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="application/json"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="GET"/>
        </http:response>
    </rest:response>,
    let $lists := termlist:get-term-lists()
    let $stats := termlist:get-term-list-stats($lists)
    return 
        $stats
};


(:~  
 : CREATE: restapi:add-term-list()
 : creates a new termlist in the apps data directory
 :
 : @param $data a json-object containing a name
 : @return ...
 : 
 : @version 0.3 (2018-07-11)
 : @status finished
 : @author Uwe Sikora
 :)
declare
    %rest:path("terms/api/1.0/termlists")
    %rest:POST("{$data}")
    %output:method("json")
function restapi:add-term-list
    ( $data ) as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="application/json"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="POST"/>
        </http:response>
    </rest:response>,
    let $decoded := util:base64-decode($data)
    let $parsed := parse-json($decoded)
    let $name := $parsed?name
    return termlist:create-termlist($name||".xml")
};


(:~  
 : READ: restapi:term-lists-by-name()
 : get a termlist by name
 :
 : @param $name the name of the termlist
 : @return a termlist xml-object
 : 
 : @version 0.3 (2019-07-05)
 : @status finished
 : @author Uwe Sikora
 :)
declare
    %rest:path("terms/api/1.0/termlists/{$name}")
    %rest:GET
    %output:method("xml")
function restapi:term-list-by-name
    ( $name ) as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="text/xml"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="GET"/>
        </http:response>
    </rest:response>,
    termlist:get-term-list-by-name( $name )
};

(:~  
 : READ: restapi:term-list-stats-by-name()
 : get the stats of a termlist by name
 :
 : @param $name the name of the termlist
 : @return a termlist xml-object
 : 
 : @version 0.3 (2019-07-05)
 : @status finished
 : @author Uwe Sikora
 :)
declare
    %rest:path("terms/api/1.0/termlists/{$name}/stats.json")
    %rest:GET
    %output:method("json")
function restapi:term-list-stats-by-name
    ( $name ) as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="application/json"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="GET"/>
        </http:response>
    </rest:response>,
    let $termlist := termlist:get-term-list-by-name( $name )
    return 
        termlist:get-term-list-stats($termlist)
};


(:~  
 : UPDATE: restapi:update-term-lists-by-name()
 : creates a new termlist in the apps data directory
 :
 : @param $name the name of the termlist
 : @param $json a json-object containing a closed and/or ignore property
 : @return ...
 : 
 : @version 0.3 (2018-07-11)
 : @status finished
 : @author Uwe Sikora
 :)
declare
    %rest:PUT("{$json}")
    %rest:path("terms/api/1.0/termlists/{$name}")
    %output:method("json")
function restapi:update-term-lists-by-name
    ( $json, $name ) as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="application/json"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="PUT POST"/>
        </http:response>
    </rest:response>,
    let $decoded := util:base64-decode($json)
    let $parsed := parse-json($decoded)
    let $termlist := termlist:get-term-list-by-name( $name )
    let $closed := if ($parsed?closed) then ( termlist:set-term-list-closed-status($termlist, $parsed?closed) ) else ()
    let $ignore := if ($parsed?ignore) then ( termlist:set-term-list-ignore-status($termlist, $parsed?ignore) ) else ()
    return termlist:get-term-list-stats($termlist)
};


(:~  
 : DELETE: restapi:delete-term-lists-by-name()
 : creates a new termlist in the apps data directory
 :
 : @param $name the name of the termlist
 : @param $json a json-object containing a closed and/or ignore property
 : @return ...
 : 
 : @version 0.3 (2018-07-11)
 : @status finished
 : @author Uwe Sikora
 :)
declare
    %rest:DELETE
    %rest:path("terms/api/1.0/termlists/{$name}")
    %output:method("json")
function restapi:delete-term-list-by-name
    ( $name ) as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="application/json"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="DELETE"/>
        </http:response>
    </rest:response>,
    if( termlist:test-term-list-exists($name) ) then (
        termlist:delete-termlist( $name )
    ) else ()
    
};