xquery version "3.1";
(:~  
 : REST-API Module for CAB-Service ("restapi", "http://dev.arokis.com/exist/term_expansion/cab_api")
 : *******************************************************************************************
 : This is the API module defining a REST-API to the term expansion app. It is used to communicate with the CAB-Webservice
 :
 : @version 0.2 (2018-07-10)
 : @status develop
 : @author Uwe Sikora
 :)

module namespace restapi="http://dev.arokis.com/exist/term_expansion/cab_api";
import module namespace cab="http://dev.arokis.com/exist/term_expansion/cab" at "xmldb:exist:///db/apps/term/modules/cab.xqm";
import module namespace console="http://exist-db.org/xquery/console";
import module namespace rest = "http://exquery.org/ns/restxq";

declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";
(:############################# Modules Variables #############################:)

(:############################# Modules Functions #############################:)


declare
    %rest:path("texpan/cab")
    %rest:POST("{$body}")
    %rest:query-param( "fmt", "{$fmt}", "raw" )
    %rest:query-param( "ofmt", "{$ofmt}", "json" )
    %rest:query-param( "a", "{$a}", "default" )
function restapi:cabify-text
    ( $body, $fmt , $ofmt , $a ) as item()* {
    
    let $encoded-body := (try {util:base64-decode($body)} catch * {$body})
    let $data := (
        try {
            replace(fn:string-join( (fn:parse-xml($encoded-body)//text()) , " "), "\s{2}", " ")
        } catch * {
            $encoded-body
        }
    )
    let $params := string-join( ("fmt="||$fmt, "ofmt="||$ofmt, "a="||$a), "&amp;" )
    return cab:http-post($data, $params)/body
};
