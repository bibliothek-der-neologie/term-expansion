xquery version "3.1";
(:~  
 : REST-API Module to handle RESOURCES ("restapi", "http://dev.arokis.com/exist/term_expansion/resources_api")
 : *******************************************************************************************
 : This is the API module defining a REST-API to the texpan app. It is used to handle resources (add, remove, synchronise etc.)
 :
 : @version 0.2 (2018-07-10)
 : @status develop
 : @author Uwe Sikora
 :)

module namespace restapi="http://dev.arokis.com/exist/term_expansion/resources_api";
import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
import module namespace indexer="http://dev.arokis.com/exist/term_expansion/indexer" at "xmldb:exist:///db/apps/term/modules/indexer.xqm";
import module namespace elements="http://dev.arokis.com/exist/term_expansion/elements" at "xmldb:exist:///db/apps/term/modules/element-constructors.xqm";
import module namespace console="http://exist-db.org/xquery/console";
import module namespace rest = "http://exquery.org/ns/restxq";
import module namespace transform="http://exist-db.org/xquery/transform";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";
declare namespace http="http://expath.org/ns/http-client";


(: ##################################################################################################
 : 
 :          TERMS
 : 
 : ############################################################################################### :)


declare
    %rest:GET
    %rest:path("terms/api/1.0/terms")
    %rest:query-param("query", "{$query}", '')
    %output:method("xml")
function restapi:get-all-terms
    ( $query ) as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="text/xml"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="GET"/>
        </http:response>
    </rest:response>,
    let $terms := if( $query = "" ) then (
            main:get-terms()
        ) else (
            let $parsed := parse-json( $query )
            return (
               console:log( util:uuid() ),
               main:get-term-opt( $parsed?term, $parsed?word, $parsed?tag )
            )
        )
     
    let $count := count( $terms )
    return elements:element-constructor( "terms", ( attribute {"count"}{ $count } ), $terms )
};

declare
    %rest:path("terms/api/1.0/terms.json")
    %rest:GET
    %output:method("json")
function restapi:get-all-terms-json
    () as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="application/json"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="GET"/>
        </http:response>
    </rest:response>,
    let $terms := main:get-terms()
    for $term in $terms
    return map{
        "term": data($term/text()),
        "lemma": data($term/@lemma),
        "word": data($term/@word),
        "tag": data($term/@tag),
        "key": data($term/@key),
        "when": data($term/@when)
    }
};

declare
    %rest:path("terms/api/1.0/terms")
    %rest:POST( "{$json-list}" )
    %output:method("xml")
function restapi:add-term
    ( $json-list ) as item()* {
    try {
        <rest:response>
            <http:response status="200">
              <http:header name="Content-Type" value="text/xml"/>
              <http:header name="Access-Control-Allow-Origin" value="*"/>
              <http:header name="Access-Control-Allow-Methods" value="POST"/>
            </http:response>
        </rest:response>,
        let $decoded := util:base64-decode(  $json-list )
        let $parsed := parse-json($decoded)
        
        for $item in $parsed?*
        let $term := elements:element-constructor("term", 
                (
                    attribute {"lemma"} {$item?lemma},
                    attribute {"word"} {$item?word},
                    attribute {"tag"} {$item?tag},
                    attribute {"ignore"} { if($item?ignore) then($item?ignore) else ("false")}
                ),
                $item?term
            )
        return indexer:index-term("system", $term)
        
    } catch *{
        <ts:error>The data is not JSON!</ts:error>
    }
    
};


declare
    %rest:GET
    %rest:path("terms/api/1.0/terms/{$key}")
    %output:method("xml")
function restapi:get-term-xml
    ( $key ) as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="text/xml"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="GET"/>
        </http:response>
    </rest:response>,
    let $terms := main:get-terms()[@key = $key]
     
    let $count := count( $terms )
    return elements:element-constructor(
        "terms", 
        ( attribute {"count"}{ $count } ), 
        $terms
    )
};


declare
    %rest:PUT("{$term-json}")
    %rest:path("terms/api/1.0/terms/{$key}")
    %output:method("xml")
function restapi:update-term
    ( $term-json, $key ) as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="text/xml"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="POST PUT"/>
        </http:response>
    </rest:response>,
    let $decoded := util:base64-decode( $term-json )
    let $parsed := parse-json( $decoded )
    let $entry := main:get-terms()[@key = $key]
    let $term := elements:element-constructor("term", 
            (
                attribute {"lemma"} {$parsed?lemma},
                attribute {"word"} {$parsed?word},
                attribute {"tag"} {$parsed?tag},
                attribute {"ignore"} { if($parsed?ignore) then($parsed?ignore) else ("false")},
                attribute {"key"} { $key },
                attribute {"when"} { current-dateTime() }
            ),
            $parsed?term
        )
    return 
        if ( $entry and not( main:get-term( data($term/text()), data($term/@word), data($term/@tag)) ) ) then (
            update replace $entry with $term,
            $term
        ) else ()

};


declare
    %rest:DELETE
    %rest:path("terms/api/1.0/terms/{$key}")
function restapi:delete-term
    ( $key ) as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="text/xml"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="GET"/>
        </http:response>
    </rest:response>,
    let $entry := main:get-terms()[@key = $key]
    return 
        if ($entry) then (
            update delete $entry,
            <cleaned>{ $key }</cleaned>
        ) else ()
};


(: ##################################################################################################
 : 
 :          WORDS
 : 
 : ############################################################################################### :)


declare
    %rest:GET
    %rest:path("terms/api/1.0/words")
    %rest:produces("text/xml")
    %output:method("xml")
function restapi:get-words
    () as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="text/xml"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="GET"/>
        </http:response>
    </rest:response>,
    let $words := main:get-words( )
    let $count := count( $words )
    return elements:element-constructor(
        "words", 
        ( attribute {"count"}{ $count } ), 
        $words
    )
};


declare
    %rest:GET
    %rest:path("terms/api/1.0/words.json")
    %rest:produces("application/json")
    %output:method("json")
function restapi:get-words-json
    () as item()* {
    <rest:response>
            <http:response status="200">
              <http:header name="Content-Type" value="application/json"/>
              <http:header name="Access-Control-Allow-Origin" value="*"/>
              <http:header name="Access-Control-Allow-Methods" value="GET"/>
            </http:response>
        </rest:response>,
    let $variants := main:get-words( )
    for $variant in $variants
    let $alternants := for $term in $variant/ts:term return map{
        "term": data($term/text()),
        "lemma": data($term/@lemma),
        "tag": data($term/@tag)
    }
    
    return 
        map{
            "word" : $variant/@key,
            "variants" : $alternants
        }
};


(: ##################################################################################################
 : 
 :          SYNONYMS
 : 
 : ############################################################################################### :)


declare
    %rest:GET
    %rest:path("terms/api/1.0/synonyms.txt")
    %rest:produces("text/plain")
    %output:method("text")
function restapi:get-synonym-txt
    () as item()* {
    
    let $synonyms := main:get-synonym-list()
    for $synonym in $synonyms
    return (
        <rest:response>
            <http:response status="200">
              <http:header name="Content-Type" value="text/plain"/>
              <http:header name="Access-Control-Allow-Origin" value="*"/>
              <http:header name="Access-Control-Allow-Methods" value="GET"/>
            </http:response>
        </rest:response>,
        $synonym || "&#xA;"
    )
};
