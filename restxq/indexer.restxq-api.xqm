xquery version "3.1";
(:~  
 : REST-API Module to handle INDEXER ("indexerAPI", "http://dev.arokis.com/exist/term_expansion/indexerapi")
 : *******************************************************************************************
 : This is the API module defining a RESTXQ-API to the indexer.
 :
 : @version 0.2 (2019-07-08)
 : @status develop
 : @author Uwe Sikora
 :)

module namespace indexerapi="http://dev.arokis.com/exist/term_expansion/indexerapi";
import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
import module namespace termlist="http://dev.arokis.com/exist/term_expansion/termlist" at "xmldb:exist:///db/apps/term/modules/termlists.xqm";
import module namespace indexer="http://dev.arokis.com/exist/term_expansion/indexer" at "xmldb:exist:///db/apps/term/modules/indexer.xqm";
import module namespace console="http://exist-db.org/xquery/console";
import module namespace rest = "http://exquery.org/ns/restxq";
import module namespace transform="http://exist-db.org/xquery/transform";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";
declare namespace http="http://expath.org/ns/http-client";


(:~  
 : READ: indexerapi:term-lists()
 : lists all termlists of the list
 :
 : @return a json-object listing all termlists
 : 
 : @version 0.3 (2018-07-11)
 : @status finished
 : @author Uwe Sikora
 :)
declare
    %rest:path("terms/api/1.0/indexer")
    %rest:GET
    %output:method("json")
function indexerapi:indexer-stats
    () as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="application/json"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="GET"/>
        </http:response>
    </rest:response>,
    
    "indexer"
};

declare
    %rest:path("terms/api/1.0/indexer/index/text/plain")
    %rest:POST( "{$data}" )
    %rest:query-param("chunks", "{$chunks}", "true")
    %output:method("xml")
function indexerapi:index-data
    ( $data, $chunks ) as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="text/xml"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="POST"/>
        </http:response>
    </rest:response>,
        if (xs:boolean($chunks)) then (
            let $chunked := indexer:chunk-text-primitive($data, 800)
            let $terms := (for $chunk in $chunked return indexer:index-resources-fast( $chunk/text(), () ))
            return
                <ts:terms count="{ count($terms) }">
                    { $terms }
                </ts:terms>
        ) else (
            let $terms := indexer:index-resources-fast( $data, () )
            return 
                <ts:terms count="{ count($terms) }">
                    { $terms }
                </ts:terms>
        )
};


declare
    %rest:path("terms/api/1.0/indexer/index/text/xml/ts:chunks")
    %rest:POST( "{$data}" )
    %output:method("xml")
function indexerapi:index-tschunk-data
    ( $data ) as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="text/xml"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="POST"/>
        </http:response>
    </rest:response>,
    let $ts-chunks := $data//ts:chunk
    let $indexed := indexer:index-resources-fast( $ts-chunks/text(), () )
    return 
        <ts:terms count="{ count($indexed) }">
            { $indexed }
        </ts:terms>
};


declare
    %rest:path("terms/api/1.0/indexer/frequencies")
    %rest:GET
    %output:method("json")
function indexerapi:index-frequency
    ( $data, $format ) as item()* {
    <rest:response>
        <http:response status="200">
          <http:header name="Content-Type" value="application/json"/>
          <http:header name="Access-Control-Allow-Origin" value="*"/>
          <http:header name="Access-Control-Allow-Methods" value="GET"/>
        </http:response>
    </rest:response>,
    array{ main:indexing-timestamps() }
};