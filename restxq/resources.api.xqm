xquery version "3.1";
(:~  
 : REST-API Module to handle RESOURCES ("restapi", "http://dev.arokis.com/exist/term_expansion/resources_api")
 : *******************************************************************************************
 : This is the API module defining a REST-API to the texpan app. It is used to handle resources (add, remove, synchronise etc.)
 :
 : @version 0.2 (2018-07-10)
 : @status develop
 : @author Uwe Sikora
 :)

module namespace restapi="http://dev.arokis.com/exist/term_expansion/resources_api";
import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
import module namespace indexer="http://dev.arokis.com/exist/term_expansion/indexer" at "xmldb:exist:///db/apps/term/modules/indexer.xqm";
import module namespace console="http://exist-db.org/xquery/console";
import module namespace rest = "http://exquery.org/ns/restxq";
import module namespace transform="http://exist-db.org/xquery/transform";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";
(:############################# Modules Variables #############################:)

(:############################# Modules Functions #############################:)

declare
    %rest:GET
    %rest:path("terms/test")
    %rest:produces("application/xml")
function restapi:testing-this
    ( ) as item() {
    <h>test</h>
};


declare
    %rest:GET
    %rest:path("terms/resources/add")
    %rest:query-param("uri", "{$resource-uri}", "/db/apps/term/data/samples/griesbach.xml")
    %rest:produces("application/xml")
function restapi:register-resource
    ( $resource-uri ) as item() {
    (: let $login := xmldb:login("/db", "admin", "") :)
    let $dummy := ""
    return <registered methode="get">{ indexer:register-resource("system", $resource-uri) }</registered>
};


declare
    %rest:path("terms/resources/add")
    %rest:POST("{$body}")
function restapi:register-resources
    ( $body as xs:base64Binary ) as item()* {
    
    (:let $login := xmldb:login("/db", "admin", ""):)
    let $encoded-body := util:base64-decode($body)
    let $json := fn:parse-json($encoded-body)
    let $registered := ( for $resource in $json?* return indexer:register-resource( "system", $resource ) )
    return <registered methode="post" count="{ count( $registered[ not(self::error) ] ) }" errors="{ count( $registered[ self::error ] ) }">{ $registered }</registered>
};


declare
    %rest:GET
    %rest:path("terms/resources/synch")
    %rest:query-param("uri", "{$resource-uri}", "/db/apps/term/data/samples/test.xml")
    %rest:produces("application/xml")
function restapi:synch-resource
    ( $resource-uri ) as item() {
    let $login := xmldb:login("/db", "admin", "")
    let $dummy := ""
    let $synchronise := indexer:synchronise-resource-by-uri( $resource-uri )
    return (
        <synch methode="get">
            { 
                if ($synchronise) then ($synchronise) else ()
            }
        </synch>
    )
};


declare
    %rest:GET
    %rest:path("terms/resources/remove")
    %rest:query-param("uri", "{$resource-uri}", "/db/apps/term/data/samples/griesbach.xml")
    %rest:query-param("mode", "{$mode}", "")
    %rest:produces("application/xml")
function restapi:remove-resource
    ( $resource-uri, $mode ) as item() {
    let $login := xmldb:login("/db", "admin", "")
    return <removed>{ indexer:remove-resource-object-by-uri( $resource-uri ) }</removed>
};


declare
    %rest:GET
    %rest:path("terms/resources")
    %rest:query-param( "uri", "{$resource-uri}" )
    %rest:produces("application/xml")
function restapi:list-resource
    ( $resource-uri ) as item()* {
    
    
    if ($resource-uri) then (
        <resources count="1">{ element resource { indexer:check-resource-consistency-by-uri( $resource-uri )[1]/@* } }</resources>
    ) else (
        let $resources := main:list-resource-instances()[not(@uri)]
        let $registered-resources := data( main:list-registered-resource-instances()/@uri )
        return (
            <resources count="{ count( ($resources, $registered-resources) ) }">
                {
                    $resources, 
                    for $resource in $registered-resources return element resource { indexer:check-resource-consistency-by-uri( $resource )[1]/@* }
                }
            </resources>
        )
    )
    
};


declare
    %rest:GET
    %rest:path("terms/resources/index")
    %rest:query-param( "uri", "{$resource-uri}" )
    %rest:query-param( "xslt", "{$preprocessing-xslt}", "/db/sghsd" )
    %rest:query-param( "chunk", "{$chunk}", "true" )
    %rest:produces("application/xml")
function restapi:index-resource
    ( $resource-uri, $preprocessing-xslt, $chunk ) as item()* {
    
    let $resource-data := doc($resource-uri)
    let $chunked-data := if ( $chunk ) then ( 
            transform:transform($resource-data, doc("/db/apps/term/queries/chunking.default.xsl"), <parameters><param name="chunk-node" value="div"/><param name="type" value="chapter"/></parameters>)
        ) else ( $resource-data )
    return $chunked-data//normalize-space(child::node())
    
};
