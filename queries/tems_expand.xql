xquery version "3.1";

import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
import module namespace indexer="http://dev.arokis.com/exist/term_expansion/indexer" at "xmldb:exist:///db/apps/term/modules/indexer.xqm";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";



let $context := "system"
let $resources := ("Das täst izt ein erster täst, op das aucj so functioniert", "Auf der andren Seite find ich scho dass das irgendwie anders sein sol andren plöd")
let $terms := main:get-terms()
(:  :let $index := indexer:index-resources( $resources ):)
(:  :let $test := main:list-resource-instances( $context ):)
(:let $resource-uri := "/db/apps/term/data/samples/griesbach-short.xml":)
(:let $chunked-resource-uri := "/db/apps/term/data/samples/griesbach_short.chunks.xml":)
let $process := main:process-strings-by-cab( $resources )

return $process

