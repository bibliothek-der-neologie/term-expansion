xquery version "3.1";

import module namespace documentation="http://dev.arokis.com/exist/term_expansion/documentation" at "xmldb:exist:///db/apps/term/modules/documentation.xqm";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";

let $articles := documentation:get-articles()
let $ids := documentation:get-articles-ids()
let $a-b-t := documentation:group-articles-by-tag()
let $tags := documentation:get-articles-tags()
return $a-b-t