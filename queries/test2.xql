xquery version "3.1";

import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
import module namespace indexer="http://dev.arokis.com/exist/term_expansion/indexer" at "xmldb:exist:///db/apps/term/modules/indexer.xqm";

let $context := "system"
let $resources := ("Das izt ein erster täst, op das aucj so functioniert", "Auf der andren Seite find ich scho dass das irgendwie anders sein sol")
let $term := main:get-terms( $context )
(:  :let $index := indexer:index-resources($context, $resources, ())
let $test := main:list-resource-instances( $context ):)
let $resource-uri := "/db/apps/term/data/samples/griesbach-short.xml"
let $chunked-resource-uri := "/db/apps/term/data/samples/griesbach_short.chunks.xml"
let $online-res := "https://tei-c.org/release/xml/tei/custom/odd/tei_tite.odd"

(:  :let $document := doc($chunked-resource-uri)
let $register := indexer:index-registered-resource($context, $resource-uri, "jh", <e/>)
let $ana := main:analyse-resources-by-uri($online-res, false())
let $register := indexer:index-registered-resource($context, $online-res, "jh", <e/>):)
(:  :let $rem := indexer:remove-resource-object-by-uri( $online-res ):)
let $cons := indexer:check-resource-consistency-by-uri( $online-res )
let $status := indexer:set-resource-index-status-by-uri( $resource-uri, false() )
let $sync := indexer:synchronise-resource-by-uri( $resource-uri )
return $cons