xquery version "3.1";

import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
import module namespace indexer="http://dev.arokis.com/exist/term_expansion/indexer" at "xmldb:exist:///db/apps/term/modules/indexer.xqm";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";



declare function local:times() as item()*{
    let $terms := main:get-terms()
    let $distinct-values := distinct-values(data($terms/@when))
    for $value in $distinct-values
    return map{"when" : $value, "count" : count($terms[@when = $value])}
};


main:indexing-timestamps()