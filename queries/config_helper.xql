xquery version "3.1";


import module namespace helper="http://dev.arokis.com/exist/helper" at "xmldb:exist:///db/apps/term/modules/helper.xqm";
import module namespace appconf="http://dev.arokis.com/exist/term_expansion/appconf" at "xmldb:exist:///db/apps/term/modules/app-conf.xqm";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";




let $config := appconf:get-param("app_data-collection")


return appconf:get-config-api('cab')/url/text()

