xquery version "3.1";

import module namespace store="http://dev.arokis.com/exist/term_expansion/store" at "xmldb:exist:///db/apps/term/modules/datastore.xqm";
import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";

let $data-store := store:collection()/ts:texpan
let $terms := main:get-distinct-terms()
let $words := main:group-terms-by-word( $terms )
let $lemma := main:group-terms-by-lemma( $terms )
return main:get-lemmata()