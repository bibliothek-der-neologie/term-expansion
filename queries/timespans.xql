xquery version "3.1";

import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
import module namespace indexer="http://dev.arokis.com/exist/term_expansion/indexer" at "xmldb:exist:///db/apps/term/modules/indexer.xqm";
import module namespace termlist="http://dev.arokis.com/exist/term_expansion/termlist" at "xmldb:exist:///db/apps/term/modules/termlists.xqm";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";


declare function local:terms-by-date
    (){
    for $term in collection($main:system-data-root)//ts:term
    let $when := $term/@when
    group by $when
    return 
        <ts:date when="{ $when }" sum="{ count($term) }">
            { $term }
        </ts:date>
    
};

declare function local:resources-by-date
    (){
    for $resource in main:get-resources-chunks()//ts:resource
    let $when := $resource/@when
    group by $when
    return 
        <ts:date when="{ $when }" sum="{ sum($resource/@token) }" resources="{ count($resource) }">
            { $resource }
        </ts:date>
    
};

declare function local:date-stats
    (){
    let $terms-dates := local:terms-by-date()
    let $resources-dates := local:resources-by-date()
    for $date in $terms-dates
    let $token := $resources-dates[@when = $date/@when]/@sum
    let $resources := count( $resources-dates[@when = $date/@when]/ts:resource )
    return map:new((
        map:entry("when", data($date/@when) ), 
        map:entry("indexed", data($date/@sum) ),
        map:entry("token", if ($token) then (data($token)) else ("0")),
        map:entry("resources", if ($resources) then ($resources) else ("0"))
    ))
    
};

let $term-dates := collection($main:system-data-root)//@when[parent::ts:term]/parent::node()
let $resource-dates := collection($main:system-data-root)//@when[parent::ts:resource]/parent::node()

(:return local:resources-by-date():)
return local:date-stats()
