xquery version "3.1";

import module namespace main="http://dev.arokis.com/exist/term_expansion/main" at "xmldb:exist:///db/apps/term/modules/main.xqm";
import module namespace indexer="http://dev.arokis.com/exist/term_expansion/indexer" at "xmldb:exist:///db/apps/term/modules/indexer.xqm";
declare namespace ts="http://dev.arokis.com/exist/term_expansion/schema";




let $terms := main:get-terms()

for $term in $terms
where $term/@lang
return $term

